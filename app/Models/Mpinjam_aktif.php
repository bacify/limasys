<?php

namespace App\Models;

use CodeIgniter\Model;

class Mpinjam_aktif extends Model
{
    protected $table      = 'pinjam_aktif';
    protected $primaryKey = 'id_pinjam';
    protected $useTimestamps = false;
    protected $useSoftDeletes = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_pinjam','id_detail','member_id','admin_id','katalog_id','status'];
}