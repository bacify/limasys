<?php

namespace App\Models;

use CodeIgniter\Model;

class Mpinjaman extends Model
{
    protected $table      = 'pinjam';
    protected $primaryKey = 'id_pinjam';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_pinjam','member_id','admin_id','keterangan','status_pinjam'];
}