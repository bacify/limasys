<?php

namespace App\Models;

use CodeIgniter\Model;

class Muser extends Model
{
    protected $table      = 'admin';
    protected $primaryKey = 'id_admin';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_admin','nama','username','email','password','akses','status'];
}