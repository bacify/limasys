<?php

namespace App\Models;

use CodeIgniter\Model;

class Mtransaksi_detail extends Model
{
    protected $table      = 'transaksi_master_detail';
    protected $primaryKey = 'id_detail';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_detail','master_id','inventory_id','katalog_id','masuk','keluar','keterangan'];
}