<?php

namespace App\Models;

use CodeIgniter\Model;

class Mtransaksi_master extends Model
{
    protected $table      = 'transaksi_master';
    protected $primaryKey = 'id_master';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_master','admin_id','jenis_transaksi','asal','keterangan','status_transaksi'];
}