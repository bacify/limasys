<?php

namespace App\Models;

use CodeIgniter\Model;

class Mprovinsi extends Model
{
    protected $table      = 'provinces';
    protected $primaryKey = 'prov_id';
    protected $useTimestamps = false;
    protected $useSoftDeletes = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['prov_id','prov_name','location_id','status'];
}