<?php

namespace App\Models;

use CodeIgniter\Model;

class Mpinjaman_detail extends Model
{
    protected $table      = 'pinjam_detail';
    protected $primaryKey = 'id_detail';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['pinjam_id','id_detail','inventory_keluar_id','inventory_masuk_id','katalog_id','durasi','tanggal_pinjam','tanggal_kembali','admin_kembali_id','status'];
}