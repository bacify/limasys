<?php

namespace App\Models;

use CodeIgniter\Model;

class Mkatalogstok extends Model
{
    protected $table      = 'stok_inventory';
    protected $primaryKey = 'katalog_id';
    protected $useTimestamps = false;
    protected $useSoftDeletes = false;
 
    protected $allowedFields = ['katalog_id', 'data_masuk','data_keluar','stok','idx'];
}