<?php

namespace App\Models;

use CodeIgniter\Model;

class Mkota extends Model
{
    protected $table      = 'cities';
    protected $primaryKey = 'city_id';
    protected $useTimestamps = false;
    protected $useSoftDeletes = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['city_id','city_name','prov_id'];
}