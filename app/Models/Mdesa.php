<?php

namespace App\Models;

use CodeIgniter\Model;

class Mdesa extends Model
{
    protected $table      = 'subdistricts';
    protected $primaryKey = 'subdis_id';
    protected $useTimestamps = false;
    protected $useSoftDeletes = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['subdis_id','subdis_name','dis_id'];
}