<?php

namespace App\Models;

use CodeIgniter\Model;

class Mmember extends Model
{
    protected $table      = 'member';
    protected $primaryKey = 'id_member';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_member','nama','identitas','alamat','desa','kec','kota','provinsi','kodepos','telp','email','password','type','status'];
}