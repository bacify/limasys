<?php

namespace App\Models;

use CodeIgniter\Model;

class Mkec extends Model
{
    protected $table      = 'districts';
    protected $primaryKey = 'dis_id';
    protected $useTimestamps = false;
    protected $useSoftDeletes = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['dis_id','dis_name','city_id'];
}