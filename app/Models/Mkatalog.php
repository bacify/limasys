<?php

namespace App\Models;

use CodeIgniter\Model;

class Mkatalog extends Model
{
    protected $table      = 'katalog';
    protected $primaryKey = 'id_katalog';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_katalog','no_register','no_panggil','judul','pengarang','penerbit','kategori_id','bahasa','tahun','edisi','subyek','klasifikasi','deskripsi','isbn'];
}