<?php

namespace App\Models;

use CodeIgniter\Model;

class Mbooking extends Model
{
    protected $table      = 'booking';
    protected $primaryKey = 'id_booking';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_booking','member_id','tanggal_pinjam','status'];
}