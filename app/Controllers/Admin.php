<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use \Laminas\Barcode\Barcode;

use \Endroid\QrCode\Color\Color;
use \Endroid\QrCode\Encoding\Encoding;
use \Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use \Endroid\QrCode\QrCode;
use \Endroid\QrCode\Label\Label;
use \Endroid\QrCode\Label\Font\NotoSans;
use \Endroid\QrCode\Logo\Logo;
use \Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use \Endroid\QrCode\Writer\PngWriter;
 
use App\Models\Mkatalog; 
use App\Models\Mkatalogstok; 
use App\Models\Mkunjungan; 
use App\Models\Mpinjaman; 
use App\Models\Mbooking; 
use App\Models\Mmember; 
use App\Models\Mkategori; 
use App\Models\Mtransaksi_master; 
use App\Models\Mtransaksi_detail; 
use App\Models\Minventory; 
use App\Models\Muser; 
use App\Models\Mprovinsi; 
use App\Models\Mkota; 
use App\Models\Mkec; 
use App\Models\Mdesa; 
use App\Models\Mpinjam_aktif; 
use App\Models\Mpinjaman_detail; 



class Admin extends BaseController
{
	public function index()
	{
        $c=array();
		$c['MONTH(created_at)']=date('m');
        $c['YEAR(created_at)']=date('Y');
        $kj = new Mkunjungan();
		$jumlah_kunjungan=$kj->where($c)->countAll();
        $data['jumlah_kunjungan']=$jumlah_kunjungan;
        
		$c=array();
		$c['MONTH(tanggal)']=date('m');
        $c['YEAR(tanggal)']=date('Y');
        $pj= new Mpinjaman();
		$jumlah_pinjaman=$pj->where($c)->countAll();
        $data['jumlah_pinjaman']=$jumlah_pinjaman;
        
		 
        
        $Mm = new Mmember();
		$jumlah_member= $Mm->countAll();	
        $data['jumlah_member']=$jumlah_member;
        
        $Kt = new Mmember();
		$jumlah_jenis_katalog= $Kt->countAll();		 
        $data['jumlah_jenis_katalog']=$jumlah_jenis_katalog;
        
        $Kts = new Mkatalogstok();
        $inventory= $Kts->findAll();
        		 
		$total=0;
		foreach($inventory as $iv){
			$total+=$iv['stok'];
		}
		$data['jumlah_katalog']=$total;
		return view('admin_home',$data);
    }
    
     
    public function mkatalog(){
        $kt=new Mkategori();
        $kategori = $kt->findAll();
		$data['kategori']=$kategori;
		return view('admin_master_katalog',$data);
    }
    public function mkatalog_kategori(){
        $kt=new Mkategori();
        $kategori = $kt->findAll();
		$data['kategori']=$kategori;
		$data['title']='Daftar Kategori Katalog';
		return view('admin_katalog_kategori',$data);
    }
    public function mkatalog_kategori_save(){
        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [
                                'nama_kategori' => 'required']
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){

             
            $kt=new Mkategori();
            $result=$kt->save([
                "id_kategori" => $this->request->getPost('id'),                
                "nama_kategori" => $this->request->getPost('nama_kategori')   ]);
            if($result){
                if($this->request->getPost('id') == ''){                    
                    $id=$kt->getInsertID();                                       
                    //echo 'valid';
                    $session->setFlashdata('error', 'kategori dengan id '.$id.' berhasil disimpan');       
                    
                   

                }else{
                      
                     $session->setFlashdata('error', 'Perubahan data kategori berhasil dilakukan');       
                     
                }
                
            }else{
                $session->setFlashdata('error', 'Update kategori GAGAL');       
                
                
            }
            
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Update Katalog GAGAL. error:'.json_encode($errors));       
            print_r($errors);
             
        }

        return redirect()->to('/panel/kategori');
    }
    public function mkatalog_kategori_delete(){
        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [
                                'id' => 'required|numeric'                                
                                                                                              
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){

             
            $kt=new Mkategori();
            $result=$kt->delete($this->request->getPost('id'),true);
            if($result){
                $session->setFlashdata('error', 'kategori berhasil dihapus');       
             
            }else{
                $session->setFlashdata('error', 'Hapus kategori GAGAL');       
                
                
            }
            
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Hapus kategori GAGAL. error:'.json_encode($errors));       
            print_r($errors);
             
        }

        return redirect()->to('/panel/kategori');

    }
     
    public function mkatalog_kategori_ajax(){
        $db = db_connect();
        $builder = $db->table('katalog_kategori')
                        ->select('id_kategori,nama_kategori')
                        ->where('katalog_kategori.deleted_at IS NULL');
                        

         
        return DataTable::of($builder)  
                ->add('action', function($row){
                    return '<a href="javascript:void(0);" class="edit_record btn btn-transparent btn-sm text-primary" data-id="'.$row->id_kategori.'"  data-nama="'.$row->nama_kategori.'"  title="edit Kategori"><i class="far fa-edit"></i></a> 
                    <a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger" data-id="'.$row->id_kategori.'" title="Hapus Kategori"><i class="fas fa-trash"></i></a>
                    ';
                }, 'last') 
                ->hide('id_kategori')
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }   
    public function katalogajax(){
        $db = db_connect();
        $builder = $db->table('katalog')
                        ->select('id_katalog,
                        no_register,
                         no_panggil, judul,pengarang,penerbit, nama_kategori')
                        ->where('katalog.deleted_at IS NULL')
                        ->join('katalog_kategori r','r.id_kategori = kategori_id');

         
        return DataTable::of($builder)                
                ->add('action', function($row){
                    return '<a href="javascript:void(0);" class="view_record btn btn-transparent btn-sm text-info" data-id="'.$row->id_katalog.'" title="Lihat Detail Katalog"><i class="fas fa-info-circle"></i></a>
                    <a href="javascript:void(0);" class="edit_record btn btn-transparent btn-sm text-primary" data-id="'.$row->id_katalog.'"  title="edit Katalog"><i class="far fa-edit"></i></a> 
                    <a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger" data-id="'.$row->id_katalog.'" title="Hapus Katalog"><i class="fas fa-trash"></i></a>
                    ';
                }, 'last')
                ->hide('id_katalog')
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }   

    public function katalog_kategori(){
        $kt=new Mkategori();
        $kategori = $kt->findAll();
		$data['kategori']=$kategori;
		$this->admintemplate->load('admin_kategori_katalog',$data);
    }

    public function katalog_new(){

        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [
                                'judul' => 'required',                                
                                'pengarang' => 'required',
                                'penerbit' => 'required',
                                'kategori' => 'required',
                                'bahasa' => 'required'                                                    
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){

             
            $cs = new Mkatalog();
            $result=$cs->save([
                "id_katalog" => $this->request->getPost('id'),                
                "judul" => $this->request->getPost('judul'),
                "no_panggil" => $this->request->getPost('no_panggil'),
                "pengarang" => $this->request->getPost('pengarang'),
                "penerbit" => $this->request->getPost('penerbit'),
                "kategori_id" => $this->request->getPost('kategori'),
                "bahasa" => $this->request->getPost('bahasa'),
                "tahun" => $this->request->getPost('tahun'),
                "edisi" => $this->request->getPost('edisi'),
                "subyek" => $this->request->getPost('subyek'),            
                "klasifikasi" => $this->request->getPost('klasifikasi'),            
                "deskripsi" => $this->request->getPost('deskripsi'),            
                "isbn" => $this->request->getPost('isbn')
            ]);
            if($result){
                if($this->request->getPost('id') == ''){                    
                    $id=$cs->getInsertID();
                    $cs = new Mkatalog();
                    $result2=$cs->save([
                        "id_katalog" => $id,
                        "no_register" => substr($this->request->getPost('bahasa'),0,1).date('Y').sprintf('%04d',$id)
                    ]);                      
                    //echo 'valid';
                    $session->setFlashdata('error', 'Katalog dengan id '.$id.' berhasil disimpan');       
                    
                   

                }else{
                    $id=$this->request->getPost('id');
                    $cs = new Mkatalog();
                    $result2=$cs->save([
                        "id_katalog" => $id,
                        "no_register" => substr($this->request->getPost('bahasa'),0,1).date('Y').sprintf('%04d',$id)
                    ]);  
                     $session->setFlashdata('error', 'Perubahan data katalog berhasil dilakukan');       
                     
                }
                
            }else{
                $session->setFlashdata('error', 'Update Katalog GAGAL');       
                
                
            }
            
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Update Katalog GAGAL. error:'.json_encode($errors));       
            print_r($errors);
             
        }

        return redirect()->to('/panel/katalog');
    }

    public function katalog_del(){
        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [
                                'id' => 'required|numeric'                                
                                                                                              
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){

             
            $cs = new Mkatalog();
            $result=$cs->delete($this->request->getPost('id'));
            if($result){
                $session->setFlashdata('error', 'katalog berhasil dihapus');       
             
            }else{
                $session->setFlashdata('error', 'Hapus Katalog GAGAL');       
                
                
            }
            
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Hapus Katalog GAGAL. error:'.json_encode($errors));       
            print_r($errors);
             
        }

        return redirect()->to('/panel/katalog');

    }

    public function getkatalogjson($id){
        $con['id_katalog']= $id;
        $cs= new Mkatalog();    
        $result = $cs->where($con)
                    ->first();

        return json_encode($result);
    }
    
    public function katalog_stok(){
        $data['title']='Data Stok Katalog ';
		return view('admin_katalog_stok',$data);
    }

    public function katalogstokajax(){
        $db = db_connect();
        $builder = $db->table('stok_inventory')
                        ->select('idx, judul,pengarang,penerbit,nama_kategori,stok')                      
                        ->join('katalog', 'idx=katalog.id_katalog')
                        ->join('katalog_kategori r','r.id_kategori = kategori_id');

         
        return DataTable::of($builder)                
                ->add('action', function($row){
                    return '<a href="javascript:void(0);" class="qrcodebtn btn btn-outline-dark btn-sm " data-id="'.$row->idx.'" title="Cetak QRCode"><i class="fas fa-qrcode"></i></a>
                    <a href="javascript:void(0);" class="barcodebtn btn btn-outline-dark btn-sm" data-id="'.$row->idx.'" title="Cetak Barcode"><i class="fas fa-barcode"></i></a>
                    ';
                }, 'last')
                ->hide('idx')
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    } 
    public function barcode(){
        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [
                                'id' => 'required|numeric',                                
                                'type' => 'required|numeric',                                                              
                                'field' => 'required'                                                              
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $id= $this->request->getGet('id');
            $field= $this->request->getGet('field');
            $type= $this->request->getGet('type');
            $con['id_katalog']=$id;
            $cs= new Mkatalog();    
            $result = $cs->select('judul,pengarang,isbn,concat_ws(".",SUBSTRING(bahasa,1,1),YEAR(katalog.created_at),id_katalog) as noreg')
                    ->where($con)
                    ->first();

            if($field=='id'){
                /* GET ID */			
                $value=sprintf('%06d', $id);
            }else if($field=='isbn'){
                $value=$result['isbn'];
            }else if($field=='register'){
                $value=$result['noreg'];
    
            }else{
                echo 'error';
                die();
            }
            $data=array();
            if($value==''){
                $value='000000';
            }
            $data['field']=strtoupper($field);
            $data['value']=strtoupper($value);

            $con['id_katalog']= $id;

            
            if($result){
                $data['judul']=$result['judul'];
                $data['pengarang']=$result['pengarang'];
		    }
		if($type==1)
			return view('admin_qrcode_print',$data);
		else
			return view('admin_barcode_print',$data);
            
        }else{
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));       
            return redirect()->to('/panel/stok_katalog');
        }
    }
    public function getbarcode($id){

		 
		// Only the text to draw is required.
        $barcodeOptions = ['text' => $id,
                              'barHeight'=>70,
                              'barThickWidth'=>6,
                              'barThinWidth'=>2,
                           // 'factor' =>2,
                            'stretchText'=>true,
                            'fontSize' => '10'
                         ];

        
        // No required options.
        $rendererOptions = ['imageType' => 'png'];
        $renderer = Barcode::factory(
            'code128',
            'image',
            $barcodeOptions,
            $rendererOptions
        )->draw();
        header("Content-Type: image/png");
        imagepng($renderer);
        exit;      
      
    }
    public function getqrcode($id){

        $writer = new PngWriter();
         
        // Create QR code
        $qrCode = QrCode::create($id)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(500)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        // Create generic logo
        $logo = Logo::create(base_url('/assets/img/limasys_logo.png'))
            ->setResizeToWidth(80);

        // Create generic label
        $label = Label::create($id)
            ->setTextColor(new Color(0, 0, 0))
            ->setFont(new NotoSans(25));

        $result = $writer->write($qrCode, $logo, $label);

        header('Content-Type: '.$result->getMimeType());
        echo $result->getString();

        exit; 

    }

    public function inventory(){
        $data['title']="Inventaris Buku";
        return view('admin_inventory',$data);
    }

    public function inventoryajax(){

        $db = db_connect();
        $builder = $db->table('transaksi_master')
                        ->select('id_master,transaksi_master.created_at,jenis_transaksi,asal,keterangan,nama,status_transaksi')
                        ->where('transaksi_master.deleted_at IS NULL')                      
                        ->join('admin', 'admin_id=id_admin');
                         

         
        return DataTable::of($builder)                
                ->add('action', function($row){
                    if($row->status_transaksi == 0)
                        return  '<a href="'.base_url('panel/i/detail/'.$row->id_master).'" class="view_record btn btn-transparent btn-sm text-info" title="Detail Transaksi"><i class="fas fa-tasks"></i></a> '.
                            '<a href="javascript:void(0);" class="edit_record btn btn-transparent btn-sm text-primary" data-id="'.$row->id_master.'"  title="Edit Transaksi"><i class="far fa-edit"></i></a> '.
                            '<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger" data-id="'.$row->id_master.'" title="Hapus Transaksi"><i class="fas fa-trash"></i></a>';
                    else if($row->status_transaksi == 1)
                        return '<a href="'.base_url('panel/i/detail/'.$row->id_master).'" class="view_record btn btn-transparent btn-sm text-success" title="Detail Transaksi"><i class="fas fa-tasks"></i></a>';
                    else 
                        return 'N/A';
                    
                }, 'last')
                ->edit('status_transaksi', function($row){
                    if($row->status_transaksi==0)
                        return '<button class="btn btn-transparent btn-sm text-primary" readonly> <i class="fas fa-sync" title="sedang diproses"></i></button>';
                    else
                        return '<button class="btn btn-transparent btn-sm text-success" readonly><i class="far fa-check-circle" title="Sudah diproses"></i></button>';
                })
                ->edit('id_master', function($row){
                     
                    return sprintf('%06d',$row->id_master);                   
                })
                ->edit('jenis_transaksi', function($row){
                     if($row->jenis_transaksi == 1)
                        return '<span class="badge badge-success text-center">IN</span>';
                    else 
                        return '<span class="badge badge-danger text-center">OUT</span>';
                                   
                })
            //     ->hide('idx')
            //    ->addNumbering() //it will return data output with numbering on first column
               ->toJson();

         
    }

    public function inventory_new(){

        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'asal' => 'required',
                                'jenis'=> 'required'                                                                
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){

             
            $cs = new Mtransaksi_master();
            $val=[
                "id_master" => $this->request->getPost('id_master'),
                "jenis_transaksi" => $this->request->getPost('jenis'),
                "asal" => $this->request->getPost('asal'),
                "keterangan" => $this->request->getPost('keterangan')
                              
            ];
            if($this->request->getPost('id_master') == ''){                
                $val['admin_id']=session()->get('UID')  ;
            }
            
            $result=$cs->save($val);

            if($result){
                if($this->request->getPost('id_master') == ''){
                    $id=$cs->getInsertID();
                    //echo 'valid';
                    $session->setFlashdata('error', 'Katalog dengan id '.$id.' berhasil disimpan');       
                    
                   

                }else{
                     $id=$cs->getInsertID();
                     $session->setFlashdata('error', 'Perubahan data katalog berhasil dilakukan');       
                     
                }
                
            }else{
                $session->setFlashdata('error', 'Update Katalog GAGAL');       
                
                
            }
            
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Update Katalog GAGAL. error:'.json_encode($errors));       
            
             
        }

        return redirect()->to('/panel/inventory');
    }

    public function inventory_delete(){
        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [
                                'id' => 'required|numeric' 
                                ]
                                );        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
             
            $cs = new Mtransaksi_master();
            $result=$cs->delete($this->request->getPost('id'));
            if($result){
                $session->setFlashdata('error', 'Data Dihapus'); 
            }else{
                $session->setFlashdata('error', 'Data Gagal Dihapus');     
            }            
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Data Gagal Dihapus. error:'.json_encode($errors));      
        }

        return redirect()->to('/panel/inventory');
    }
    public function getinventoryjson($id){
        $con['id_master']= $id;
        $cs= new Mtransaksi_master();    
        $result = $cs->where($con)
                    ->first();

        return json_encode($result);
    }
    public function inventory_detail($id){
        $data['title']="Inventaris Buku";
        $data['id']=$id;
        $m= new Mtransaksi_master();
        $data['master']=$m->where('id_master',$id)->first();
        $kt=new Mkategori();
        $kategori = $kt->findAll();
		$data['kategori']=$kategori;
        return view('admin_inventory_detail',$data);
    }

    public function inventorydetailajax($id){

        $m = new Mtransaksi_master();
        $master = $m->find($id);

        $db = db_connect();
        $builder = $db->table('transaksi_master_detail')
                        ->select('id_detail,
                        concat_ws(".",SUBSTRING(bahasa,1,1),YEAR(katalog.created_at),id_katalog) as noreg,
                        ,no_panggil,judul,pengarang,penerbit,nama_kategori,stok,masuk,keluar,status_transaksi')
                        ->where('transaksi_master_detail.deleted_at IS NULL')                      
                        ->where('master_id',$id) 
                        ->join('transaksi_master', 'master_id=id_master')                        
                        ->join('stok_inventory', 'stok_inventory.katalog_id=transaksi_master_detail.katalog_id')                        
                        ->join('katalog', 'transaksi_master_detail.katalog_id=id_katalog')                        
		                ->join('katalog_kategori', 'kategori_id=id_kategori');                 
                        
                         

           
            $result= DataTable::of($builder)                
                ->add('action', function($row){
                    
                    if($row->status_transaksi == 0)
                        return  '<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger" data-id="'.$row->id_detail.'" title="Hapus Transaksi"><i class="fas fa-trash"></i></a>';
                    else 
                        return '';
                    
                }, 'last')
                ->edit('masuk', function($row){                    
                     
                        if($row->status_transaksi == 0)                        
                            return '<form><input type="hidden" class="istok" value="'.$row->stok.'"><input type="text" name="jumlah" class="form-control form-control-sm inputjumlah" value="'.$row->masuk.'" onchange="updateJumlah(this)"  ><input type="hidden" name="id" class="inputiddetail" value="'.$row->id_detail.'"></form>';
                        else
                            return $row->masuk;
                                          
                    
                })
                ->edit('keluar', function($row){                    
                    
                        if($row->status_transaksi == 0)                        
                            return '<form><input type="text" name="jumlah" class="form-control form-control-sm inputjumlah" value="'.$row->keluar.'" onchange="updateJumlah(this)"  ><input type="hidden" name="id" class="inputiddetail" value="'.$row->id_detail.'"></form>';
                        else
                            return $row->keluar;
                    
                        
                    
                })
                ->edit('stok', function($row){                    
                  return '<form><input type="text" name="stok" class="form-control form-control-sm istok" value="'.$row->stok.'" disabled></form>';
                
                })
                ->hide('id_detail')
                ->hide('status_transaksi')
                ->addNumbering(); //it will return data output with numbering on first column
                if($master['status_transaksi']==1)
                    $result->hide('action');
                if($master['jenis_transaksi'] == 1)
                return $result->hide('keluar')->hide('stok')->toJson();
                else
                return $result->hide('masuk')->toJson();
         
    }


    public function cari_katalog(){
        $con=$this->request->getGet('term');
		if($con==''){
			echo json_encode(array());
			die();
		}
		$like=array();
		$like['concat_ws(".",SUBSTRING(bahasa,1,1),YEAR(katalog.created_at),id_katalog)'] = $con;
		$like['no_panggil'] = $con;
		$like['judul'] = $con;
        $cs= new Mkatalog();    
        $r = $cs->select('id_katalog,
                         concat_ws(".",SUBSTRING(bahasa,1,1),YEAR(katalog.created_at),id_katalog) as noreg,
                         no_panggil,
                         judul,
                         pengarang,
                         tahun')->orLike($like)
                         ->where('deleted_at IS null')
                        ->findAll(10);
       
                $result=array();
                foreach($r as $rr){
                    $temp=array();
                    if($rr['noreg']==null||$rr['noreg']==''){
                        $rr['noreg']='N/A';
                    }
                    if($rr['no_panggil']==null||$rr['no_panggil']==''){
                        $rr['no_panggil']='N/A';
                    }
                    if($rr['pengarang']==null||$rr['pengarang']==''){
                        $rr['pengarang']='N/A';
                    }
                    $temp=array('value'=>$rr['id_katalog'], 'label'=>$rr['noreg'].'| '.$rr['no_panggil'].'| '.$rr['judul'].'| '.$rr['pengarang'].'| '.$rr['tahun']);
                    $result[]=$temp;
                };
                

        header('Content-Type: application/json');
		echo json_encode($result);
    }
    public function cari_katalog_instock(){
        $con=$this->request->getGet('term');
		if($con==''){
			echo json_encode(array());
			die();
		}
		$like=array();
		$like['concat_ws(".",SUBSTRING(bahasa,1,1),YEAR(katalog.created_at),id_katalog)'] = $con;
		$like['no_panggil'] = $con;
		$like['judul'] = $con;
        $cs= new Mkatalog();    
        $r = $cs->select('id_katalog,
                         concat_ws(".",SUBSTRING(bahasa,1,1),YEAR(katalog.created_at),id_katalog) as noreg,
                         no_panggil,
                         judul,
                         pengarang,
                         stok,
                         tahun')->orLike($like)
                         ->where('deleted_at IS null')
                         ->join('stok_inventory','katalog.id_katalog=stok_inventory.katalog_id')
                         ->where('stok >','0')
                        ->findAll(10);
       
                $result=array();
                foreach($r as $rr){
                    $temp=array();
                    if($rr['noreg']==null||$rr['noreg']==''){
                        $rr['noreg']='N/A';
                    }
                    if($rr['no_panggil']==null||$rr['no_panggil']==''){
                        $rr['no_panggil']='N/A';
                    }
                    if($rr['pengarang']==null||$rr['pengarang']==''){
                        $rr['pengarang']='N/A';
                    }
                    $temp=array('value'=>$rr['id_katalog'], 'label'=>$rr['noreg'].'| '.$rr['no_panggil'].'| '.$rr['judul'].'| '.$rr['pengarang'].'| '.$rr['tahun'].'| '.$rr['stok']);
                    $result[]=$temp;
                };
                

        header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function detail_new(){

        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'id_master' => 'required|numeric',                                                                
                                'id_katalog' => 'required|numeric',
                                'jumlah' => 'required|numeric'                                                                
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $con=array();
            $con['master_id']=$this->request->getPost('id_master');
            $con['katalog_id']=$this->request->getPost('id_katalog');

            $td= new Mtransaksi_detail();

            $td_val = $td->where($con)->findAll();

                      
            if(count($td_val) == 0){
                $master = new Mtransaksi_master();
                $mval = $master->where('id_master',$con['master_id'])->first();
                
                if($mval['jenis_transaksi']== 1)
                    $con['masuk'] = $this->request->getPost('jumlah');
                else if($mval['jenis_transaksi']== 2)
                {   
                    // Cek Stok 
                    $st= new Mkatalogstok();
                    $stok=$st->where('katalog_id',$con['katalog_id'])->first();
                    if($stok['stok']<$this->request->getPost('jumlah')){                        
                        $session->setFlashdata('error', 'Jumlah Melebihi Stok'); 
                        return redirect()->to('/panel/i/detail/'.$con['master_id']);    
                    }                        
                    else{
                        $con['keluar'] = $this->request->getPost('jumlah');
                    }
                    
                }
                else{
                    $session->setFlashdata('error', 'Transaksi Utama Tidak Ditemukan'); 
                    return redirect()->to('/panel/i/detail/'.$con['master_id']);
                }
                
                $x=new Mtransaksi_detail();                
                $result= $x->save($con);

                if($result){
                    if($this->request->getPost('id_master') == ''){
                        $id=$x->getInsertID();
                        //echo 'valid';
                        $session->setFlashdata('error', 'Data berhasil disimpan');       
                        
                       
    
                    }else{
                         
                         $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');       
                         
                    }
                    
                }else{
                    $session->setFlashdata('error', 'Update Data GAGAL');       
                    
                    
                }
            }else{
                $session->setFlashdata('error', 'Data Sudah Ada');   
            }           
 
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Update Katalog GAGAL. error:'.json_encode($errors));      
            
             
        }

        return redirect()->to('/panel/i/detail/'.$this->request->getPost('id_master'));
    }

    public function detail_update(){
        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'id_detail' => 'required|numeric'                                                            
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        $r['status']=0;
        $r['text']='';
        if($isDataValid){
            $con=array();
            $con['id_detail']=$this->request->getPost('id_detail');
            if($this->request->getPost('masuk'))
            $con['masuk']=$this->request->getPost('masuk');
            if($this->request->getPost('keluar'))
            $con['keluar']=$this->request->getPost('keluar');

             
            $td= new Mtransaksi_detail();

            $result = $td->save($con) ;

                if($result){
                    if($this->request->getPost('id_detail') == ''){
                        $id=$td->getInsertID();
                        //echo 'valid';
                        $r = [
                            'text'=>'Data berhasil disimpan',
                            'status'=>1];       
                        
                       
    
                    }else{
                        $r = [
                            'text'=>'Update Berhasil',
                            'status'=>1];
                    }
                    
                }else{
                    $r = [
                        'text'=>'Update Gagal',
                        'status'=>0];
                    
                }
                       
 
        }
        else{
            $errors = $validation->getErrors();
                   
            $r = [
                'text'=>'Gagal '.json_encode($errors),
                'status'=>0];
             
        }
        echo json_encode($r);
    }
    public function detail_delete(){
        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [
                                'id_detail' => 'required|numeric'                                
                                                                                              
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){

             
            $cs = new Mtransaksi_detail();
            $result=$cs->delete($this->request->getPost('id_detail'));
            if($result){
                $session->setFlashdata('error', 'Data Dihapus');       
             
            }else{
                $session->setFlashdata('error', 'Data Gagal Dihapus');       
                
                
            }
            
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Data Gagal Dihapus. error:'.json_encode($errors));       
             
             
        }

        return redirect()->to('/panel/i/detail/'.$this->request->getPost('id_master'));

    }

    public function detail_katalog_new(){

        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [
                                'judul' => 'required',                                
                                'pengarang' => 'required',
                                'penerbit' => 'required',
                                'kategori' => 'required',
                                'id_master' => 'required|numeric',                                
                                'jumlah' => 'required|numeric'                                                                        
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){

            $cs = new Mkatalog();
            $result=$cs->save([                
                "no_register" => $this->request->getPost('no_register'),
                "judul" => $this->request->getPost('judul'),
                "no_panggil" => $this->request->getPost('no_panggil'),
                "pengarang" => $this->request->getPost('pengarang'),
                "penerbit" => $this->request->getPost('penerbit'),
                "kategori_id" => $this->request->getPost('kategori'),
                "bahasa" => $this->request->getPost('bahasa'),
                "tahun" => $this->request->getPost('tahun'),
                "edisi" => $this->request->getPost('edisi'),
                "subyek" => $this->request->getPost('subyek'),            
                "klasifikasi" => $this->request->getPost('klasifikasi'),            
                "deskripsi" => $this->request->getPost('deskripsi'),            
                "isbn" => $this->request->getPost('isbn')
            ]);
            if($result){                
                    
                    $id=$cs->getInsertID();
                    $cs = new Mkatalog();
                    $result2=$cs->save([
                        "id_katalog" => $id,
                        "no_register" => substr($this->request->getPost('bahasa'),0,1).date('Y').sprintf('%04d',$id)
                    ]);      
                    //echo 'valid';                                           
                    $con=array();
                    $con['master_id']=$this->request->getPost('id_master');
                    $con['katalog_id']=$id;                
                    $con['masuk'] = $this->request->getPost('jumlah');           
                    $x=new Mtransaksi_detail();                
                    $rr= $x->save($con);
                    if($rr)
                        $session->setFlashdata('error', 'Data Berhasil Disimpan');   
                    else
                        $session->setFlashdata('error', 'Data Gagal Disimpan');   
                
                
            }else{
                $session->setFlashdata('error', 'Update Katalog GAGAL');       
                
                
            }
            
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Update Katalog GAGAL. error:'.json_encode($errors));       
              
             
        }

        return redirect()->to('/panel/i/detail/'.$this->request->getPost('id_master'));
    }

    public function detail_save(){

        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [
                                 'id_master' => 'required|numeric'                                                                    
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        $master=0;
        if($isDataValid){
            $master=$this->request->getPost('id_master'); 			
		 
            if( $this->request->getPost('id_master') !== NULL && $this->check_inventory_status()){
                /* start update and prevent another update */
                $this->inventory_start();

                /* GET all detail */
                $con=array();
                $con['master_id']=$master;
                $md = new Mtransaksi_detail();
                $mdetail = $md->where($con)->findAll();


                if(count($mdetail)>0){
                    $keterangan='IN '.sprintf('%06d',$master);
                    $in = new Minventory();
                    // CHECK TO PREVENT DUPLICATE
                    $iv=$in->where('keterangan',$keterangan)->findAll();
                    if(count($iv)>0){
                        $session->setFlashdata('error', 'Ada Yang Salah, Data Sudah Masuk'); 
                        $this->inventory_end();                        
                    }else{
                        /* INSERT INTO INVENTORY */
                        $dataIn=array();
                        $tra=new Mtransaksi_master();
                        $te=$tra->where('id_master',$master)->first();                        
                        $iv = new Minventory();
                        foreach($mdetail as $vdata){
                            $text='';
                            if($te['jenis_transaksi']== 1)
                                $text='IN '.sprintf('%06d',$vdata['master_id']);                          
                            else
                                $text='OUT '.sprintf('%06d',$vdata['master_id']);
                            $val=array();
                            $val['katalog_id']=$vdata['katalog_id'];
                            $val['masuk']=$vdata['masuk'];
                            $val['keluar']=$vdata['keluar'];                            
                            $val['keterangan']=$text;
                            $iv->save($val);                                
                        }

                        /* UPDATE MASTER STATUS */                       
                        $val=array();
                        $val['id_master']=$master;
                        $val['status_transaksi']=1;
                        $m = new Mtransaksi_master();
                        $r =$m->save($val);                        
                        if($r){
                            $session->setFlashdata('error', 'Data Berhasil Disimpan');                             
                        }else{
                            $session->setFlashdata('error', 'Data Gagal Disimpan');                             
                        }


                        /* END update and prevent another update */
                        $this->inventory_end();				 
                         
                    }
                }else{
                    $this->inventory_end();
                    $session->setFlashdata('error', 'Tak bisa disimpan, BELUM ADA DATA');                        
                }
            }else{
                $session->setFlashdata('error', 'Tak bisa disimpan, Ada Proses Update yang sedang berjalan');   
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Update Katalog GAGAL. error:'.json_encode($errors));       
        }

        return redirect()->to('/panel/i/detail/'.$this->request->getPost('id_master'));
 
    }

    public function inventory_start(){
	
		$db      = \Config\Database::connect();
        $builder = $db->table('inventory_status');
        $val['status']=1;         
        $builder->update($val);
	}
	public function inventory_end(){

        $db      = \Config\Database::connect();
        $builder = $db->table('inventory_status');
        $val['status']=0;         
        $builder->update($val);
	}
	public function check_inventory_status(){

        $db      = \Config\Database::connect();
        $builder = $db->table('inventory_status');       
		$query = $builder->get()->getResult()[0];
        
		if($query->status=='0')
			return true;
		else
			return false;

    }
    

    public function inventory_history(){
        $data['title'] ='Riwayat Transaksi Inventaris';
        
        return view('admin_inventory_history',$data);
    }
    public function inventory_history_ajax(){
        $db = db_connect();
        $builder = $db->table('inventory')
                        ->select('katalog_id,inventory.created_at,judul,pengarang,penerbit,nama_kategori,masuk,keluar,keterangan')
                        ->join('katalog', 'katalog_id=id_katalog')
		                ->join('katalog_kategori', 'kategori_id=id_kategori');
                         

         
        return DataTable::of($builder)   
               ->hide('katalog_id')
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }
   

    public function kunjungan(){
        $data['title'] ='Riwayat Kunjungan Member';
        
        return view('admin_kunjungan',$data);
    }
    public function kunjunganajax(){
        $db = db_connect();
        $builder = $db->table('kunjungan')
                        ->select('id_kunjungan,kunjungan.created_at,nama')
                        ->join('member', 'member_id=id_member');
                         

         
        return DataTable::of($builder)   
               ->hide('id_kunjungan')
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }
    

    public function pinjaman_member(){
        $data['title'] = 'Daftar Pinjam Buku';
        return view('admin_pinjam_list',$data);
    }

    public function pinjaman_member_ajax(){
        $db = db_connect();
        $builder = $db->table('pinjam')
                        ->select('concat(date_format(pinjam.created_at,"%Y%m"),LPAD(id_pinjam,4,0)) as pid,id_pinjam,pinjam.created_at,nama,telp,email,status_pinjam')
                        ->where('pinjam.deleted_at IS NULL')
                        ->join('member', 'member_id=id_member');
                         

         
        return DataTable::of($builder)   
                ->add('action', function($row){
                    $text = '<a href="'.base_url('panel/p/detail/'.$row->id_pinjam).'" class="btn btn-transparent btn-sm  text-info"  title="Proses Data"><i class="fas fa-tasks"></i></a> ';
                    if($row->status_pinjam == 0)											   
                    $text .='<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger" data-id="'.$row->id_pinjam.'" title="Hapus Data"><i class="fas fa-trash"></i></a>';

                    return $text;
                }, 'last')
                ->edit('status_pinjam', function($row){
                    if($row->status_pinjam==0)
                        return '<button class="btn btn-transparent btn-sm text-info" readonly> <i class="fas fa-sync" title="Pinjaman Sedang Dibuat"></i></button>';
                    else if($row->status_pinjam==1)
                        return '<button class="btn btn-transparent btn-sm text-danger" readonly> <i class="fas fa-exclamation-circle" title="Masih Meminjam"></i></button>';
                    else
                        return '<button class="btn btn-transparent btn-sm text-success" readonly><i class="far fa-check-circle" title="Sudah Dikembalikan"></i></button>';
                })
               ->hide('id_pinjam')
               //->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }

    public function pinjaman_baru(){
        if( $this->request->getPost('submit') !== NULL){
			$session = session();
			/* CHECK IF EXIST */
			$con=array();
            $con['member_id']=$this->request->getPost('nomor');
                $pa=new Mpinjam_aktif();
                $v=$pa->where($con)->findAll();
			if(count($v)>0){
                $session->setFlashdata('error', 'Ada Tanggungan, Tidak Bisa membuat pinjaman baru');
			}else{

                $con=array();
                $con['member_id']=$this->request->getPost('nomor');
                $con['status_pinjam']=0;
                $pi=new Mpinjaman();
                $vv=$pi->where($con)->findAll();
                if(count($vv)>0){
                    $session->setFlashdata('error', 'Ada pinjaman yg belum di proses');
                }else{
                    $bytes = random_bytes(4);			
                     
                    
                    $value=array();                  
                    $value['member_id']=$this->request->getPost('nomor');
                    $value['status_pinjam']=0;				 
                    $value['admin_id']=$session->get('UID');				 
                            
                    $p = new Mpinjaman();
                    $rr=$p->insert($value);
                    
                    if($rr)
                        $session->setFlashdata('error', 'data ditambahkan');       
                    else
                        $session->setFlashdata('error', 'data Gagal ditambahkan');       
                }
			}
			
		}
	
        return redirect()->to('/panel/pinjaman');
		
    }
    public function pinjaman_hapus(){
        if( $this->request->getPost('id') !== NULL){

            $session = session();
			/* check kondisi jika ada buku yang sedang aktif dipinjam */
			$con=array();
            $con['id_pinjam']=$this->request->getPost('id');
            $pa=new Mpinjam_aktif();
			$a=$pa->where($con)->findAll();
			if(count($a)>0){
				$session->setFlashdata('error', 'Ada Pinjaman Aktif, Pinjaman tidak dapat dihapus');
			}else{
			 
				
				$id=$this->request->getPost('id');
				$p=new Mpinjaman();
				$r=$p->delete($id);		
				if($r){
					$session->setFlashdata('error', 'Pinjaman berhasil dihapus');
				}	else{
					$session->setFlashdata('error', 'Pinjaman GAGAL dihapus');
				}
			}
		}

        return redirect()->to('/panel/pinjaman');
		
    }

    public function checkpinjamid($id){

        $con=array('id_pinjam'=>$id);
        $p=new Mpinjaman();
		$r=$p->where($con)->findAll();
		if(count($r)>0){
			return true;
		}else{
			return false;
		}
    }
    
    public function pinjaman_detail($id){
        
        $idx= date('Ym').sprintf('%04d',$id);
        $data['idx']= $idx;
        $data['title']= 'Detail Pinjam Buku '.$idx;
        $data['id']=$id;
            $pd = new Mpinjaman();
                $master=$pd->find($id);
                
			if($master!=''){
                $data['master']=$master;
				$m= new Mmember();
				$member=$m->where(array('id_member'=>$master['member_id']))->findAll();
				if(count($member)>0){
					$data['member']=$member[0];
				}
			}else{
                return redirect()->to('/panel/pinjaman');
            }
        return view('admin_pinjam_detail',$data);
    }

    public function pinjaman_detail_ajax($id){
        $db = db_connect();
        $builder = $db->table('pinjam_detail')
                        ->select('id_detail,judul,pengarang,tahun,isbn,status')
                        ->where('pinjam_detail.deleted_at IS NULL')
                        ->where('pinjam_id',$id)
                        ->join('katalog', 'katalog_id=id_katalog');;
                         

         
        return DataTable::of($builder)   
                ->add('action', function($row){
                    if($row->status==0)
                        return '<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger " data-id="'.$row->id_detail.'" title="Hapus Buku"><i class="fas fa-times"></i></a>';
                    else if($row->status==1)
                        return '<a href="javascript:void(0);" class="return_record btn btn-transparent  btn-sm  text-primary p-0 m-0" data-id="'.$row->id_detail.'" title="pengembalian"><i class="fas fa-exchange-alt"></i></a>';
                    else
                        return '<button class="btn btn-transparent btn-sm text-success" readonly><i class="far fa-check-circle" title="Sudah Dikembalikan"></i></button>';
                }, 'last')
                ->edit('status', function($row){

                    switch($row->status) {
                        case '0' : return '<a href="#" class=" btn-transparent btn-sm text-info"> <i class="fas fa-ban" title="Belum Diproses"></i></a>'; break;
                        case '1' : return '<a href="#" class=" btn-transparent btn-sm text-danger"><i class="fas fa-exclamation-circle" title="Sedang Pinjaman"></i></a>'; break;
                        case '2' : return '<a href="#" class=" btn-transparent btn-sm text-success"><i class="far fa-check-circle" title="Sudah Dikembalikan"></i></a>'; break;
                        default  : return 'N/A';
                        }
                    
                })
               ->hide('id_detail')
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }
    public function pinjaman_detail_add(){
        $master=$this->request->getPost('id_pinjam');
        $session = session();
		if( $this->request->getPost('id_katalog') !== NULL){
			
			/* CHECK IF EXIST */
			$con=array();
			$con['pinjam_id']=$this->request->getPost('id_pinjam');
            $con['katalog_id']=$this->request->getPost('id_katalog');
            $pd = new Mpinjaman_detail();
			$v=$pd->where($con)->findAll();
			if(count($v)>0){
				$session->setFlashdata('error', 'data Sudah ada');
			}else{
				 
				$value=array();
				/* CHECK LOGIN */
				$value['pinjam_id']=$this->request->getPost('id_pinjam');
				$value['katalog_id']=$this->request->getPost('id_katalog');					
				$pd= new Mpinjaman_detail();		
                $r=$pd->insert($value);			
                
                if($r)
                    $session->setFlashdata('error', 'data ditambahkan');			
                else
                $session->setFlashdata('error', 'data Gagal ditambahkan');			
			}
			 
            return redirect()->to('panel/p/detail/'.$master);
			 
		}
	

        return redirect()->to('panel/p/detail/'.$master);
    }

    

    public function pinjaman_detail_delete(){
        $idpinjam=$this->request->getPost('id_pinjam');
		if($this->request->getPost('id_detail') !== NULL){			 
			
			$session = session();
            $id=$this->request->getPost('id_detail');
            $pd = new Mpinjaman_detail();
			$r=$pd->delete($id,true);			
			 	
			if($r){
				$session->setFlashdata('error', 'berhasil dihapus');
			}	else{
				$session->setFlashdata('error', 'GAGAL dihapus');
			}
		}

		
		return redirect()->to('panel/p/detail/'.$idpinjam);
    }

    public function pinjaman_detail_add_ajax(){
        $master=$this->request->getPost('id_pinjam');
		if( $this->request->getPost('nomor') !== NULL){
			
			/* check if nomor is exist on DB */
			$searchby=$this->request->getPost('searchby');
			$nomor=$this->request->getPost('nomor');
			$con=array();
			switch($searchby){
				case 'isbn':
					$con['isbn']=$nomor; 
					break;
				case 'id':
					$con['id_katalog']=$nomor; 
					break;
				case 'register':
					$con['no_register']=$nomor; 
					break;
				case 'panggil':
					$con['no_panggil']=$nomor; 
					break;
				default :{
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Search By Harus Dipilih';
					echo json_encode($r); 
					die();
				}
            }
            $ka = new Mkatalog();
			$katalog=$ka->where($con)->findAll();
			if(count($katalog)==0){
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Data Dengan Nomor '.$nomor.' TIDAK DITEMUKAN!';
					echo json_encode($r); 
					die();
            }
            
            $kis= new Mkatalogstok();
            $katalogstok = $kis->find($katalog[0]['id_katalog']);
            if($katalogstok ==''){
                $r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Stok Katalog Tidak Tersedia';
					echo json_encode($r); 
					die();

            }

			/* CHECK IF EXIST */
			$con=array();
			$con['pinjam_id']=$this->request->getPost('id_pinjam');
            $con['katalog_id']=$katalogstok['katalog_id'];
            $pd = new Mpinjaman_detail();
			$v=$pd->where($con)->findAll();;
			if(count($v)>0){
				$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Nomor '.$nomor.' Sudah DITAMBAHKAN';
					echo json_encode($r); 
					die();
			}else{
				 
				$value=array();
				/* CHECK LOGIN */
				$value['pinjam_id']=$this->request->getPost('id_pinjam');
				$value['katalog_id']=$katalogstok['katalog_id'];					
                $pd = new Mpinjaman_detail();
				$rx=$pd->insert($value);			
				if($rx){
					$r=array();
					$r['status']=1;
					$r['msg']='Berhasil! Nomor '.$nomor.' DITAMBAHKAN';
					echo json_encode($r); 
					die();
				}else{
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! ADA YANG SALAH';
					echo json_encode($r); 
					die();
				}	
			}
			 

			
		}else{
			$r=array();
			$r['status']=0;
			$r['msg']='GAGAL! ID Tidak Ditemukan';
			echo json_encode($r);
			die();
		}
    }

    public function pinjaman_detail_proses(){
        $idpinjam=$this->request->getPost('id_pinjam');
		
		if( $this->request->getPost('id_pinjam') !== NULL){			 
			
			/* GET list BUKU yang DIpinjam */
			$c=array();
            $c['pinjam_id']=$idpinjam;
            $pd = new Mpinjaman_detail();            
			$book=$pd->where($c)->findAll();
			if(count($book)==0){
				$this->session->set_flashdata('error', 'List Buku Pinjaman Tidak Boleh Kosong');
                return redirect()->to('panel/p/detail/'.$idpinjam);
				die();
			}
		
			
			/* MASUKKAN BUKU kedalam transaksi */
			$this->inventory_start();
			foreach($book as $buku){

				/* CHECK STOK BUKU */
				$c=array();
                $c['katalog_id']=$buku['katalog_id'];
                $ks = new  Mkatalogstok();
				$r=$ks->where($c)->first();
				if($r['stok'] < 1){
					/* stok buku kosong */

					/* hapus buku dari list pinjaman */
					$x=array();
                    $x['katalog_id']=$buku['katalog_id'];
                    $x['pinjam_id']=$idpinjam;
                    $pdx= new Mpinjaman_detail();
					$pdx->where($x)->delete();
				}else{
					/* insert transaksi buku */

						$keterangan='PO '.$idpinjam;
						$c2=array();
						$c2['keterangan']=$keterangan;
						$c2['katalog_id']=$buku['katalog_id'];
                        // CHECK TO PREVENT DUPLICATE
                        $min = new Minventory();
						$iv=$min->where($c2)->findAll();
						if(count($iv)>0){
							continue;
						}else{
						/* INSERT INTO INVENTORY */						 
							$val=array();
							$val['katalog_id']=$buku['katalog_id'];
							$val['keluar']=1;
							$val['keterangan']='PO '.$idpinjam;
                            
                            $mi = new Minventory();
							$idx=$mi->insert($val);					 

                            if($idx){
                                $i = $mi->getInsertID();                            
                                /* UPDATE PINJAMAN DETAIL */
                                $v=array();
                                $v['id_detail']=$buku['id_detail'];                               
                                $v['inventory_keluar_id']=$i;
                                $v['status']=1;
                                $pdm = new Mpinjaman_detail();
                                $pdm->save($v);
                            }
						}

					
				}
				
			}
			$this->inventory_end();

			 
			/* UPDATE STATUS PINJAMAN */
			$va=array();
			$va['id_pinjam']=$idpinjam;			
            $va['status_pinjam']=1;
            $p= new Mpinjaman();
			$p->save($va);			 
		}
        return redirect()->to('panel/p/detail/'.$idpinjam);
    }

    public function pinjaman_detail_return(){
        $idpinjam=$this->request->getPost('id_pinjam');
		if( $this->request->getPost('id_detail') !== NULL){			 
			
			
			/* GET transaction detail */			
			$con=array();			 
			$con['id_detail']=$this->request->getPost('id_detail');
            $con['status']=1;
            $pd = new Mpinjaman_detail();
            $r= $pd->where($con)->findAll();			
			if(count($r)>0){
				$this->inventory_start();

				$detail=$r[0];
				/* transaksi buku masuk */
				$val=array();
				$val['katalog_id']=$detail['katalog_id'];
				$val['masuk']=1;
				$val['keterangan']='PI '.$idpinjam;
                $id=array();
                $mi = new Minventory();
                $idx=$mi->insert($val);	
				if($idx){
                    $i = $mi->getInsertID();        

				/* UPDATE PINJAMAN DETAIL */
                    $v=array();
                    $v['id_detail']=$detail['id_detail'];                    
                    $v['inventory_masuk_id']=$i;
                    $v['status']=2;
                    $v['tanggal_kembali']=date("Y-m-d H:i:s");
                    $v['admin_kembali_id']=session()->get('UID');	
                    $pdm = new Mpinjaman_detail();
                    $pdm->save($v);
                }
                $this->inventory_end();
                
			}		
				 

			/* CHECK PINJAMAN TERSISA */
			$c=array();
			$c['pinjam_id']=$idpinjam;
            $c['status!=']=2;
            $pda= new Mpinjaman_detail();
			$pinjaman=$pda->where($c)->findAll();

			if(count($pinjaman)==0){
				/* UPDATE STATUS PINJAMAN */
				$va=array();
				$va['id_pinjam']=$idpinjam;				
                $va['status_pinjam']=2;
                $p= new Mpinjaman();
				$p->save($va);	
			}
			 
        }
        
        return redirect()->to('panel/p/detail/'.$idpinjam);
    }

    public function pengembalian_member(){
        $data['title'] = 'Pengembalian Buku Pinjaman';

        return view('admin_pengembalian',$data);

    }
    public function pengembalian_history_ajax(){
        $db = db_connect();
        $builder = $db->table('pinjam_detail')
                        ->select('concat(date_format(pinjam.created_at,"%Y%m"),LPAD(id_pinjam,4,0)) as pid,date_format(tanggal_pinjam,"%d-%m-%Y") as tp,date_format(tanggal_kembali,"%d-%m-%Y") as tk,judul,pengarang,nama')
                        ->where('pinjam_detail.deleted_at IS NULL')
                        ->where('tanggal_kembali IS NOT NULL')
                        ->join('katalog', 'katalog_id=id_katalog')
                        ->join('pinjam', 'pinjam_id=id_pinjam')
                        ->join('member', 'member_id=id_member');
                         

         
        return DataTable::of($builder)                
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }

    public function pengembalian_proses(){
        
        if( $this->request->getPost('nomor') !== NULL){			 
			
			$searchby=$this->request->getPost('searchby');
			$nomor=$this->request->getPost('nomor');

			/* GET KATALOG ID */
			switch($searchby){
				case 'isbn':
					$con['isbn']=$nomor; 
					break;
				case 'id':
					$con['id_katalog']=$nomor; 
					break;
				case 'register':
					$con['no_register']=$nomor; 
					break;
				case 'panggil':
					$con['no_panggil']=$nomor; 
					break;
				default :{
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Search By Harus Dipilih';
					echo json_encode($r); 
					die();
				}
            }
            
            $ka = new Mkatalog();
			$katalog=$ka->where($con)->findAll();;
			if(count($katalog)==0){
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Data Dengan Nomor '.$nomor.' TIDAK DITEMUKAN!';
					echo json_encode($r); 
					die();
			}

			/* CHECK JIKA DITEMUKA BUKU YANG DIPINJAM */
            $con=array();        
            $con['katalog_id']=$katalog[0]['id_katalog'];			 
            $pa = new Mpinjam_aktif();
			$datakatalog=$pa->where($con)->findAll();

			$jumlah=count($datakatalog);
			if($jumlah==0){
				$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Data Dengan Nomor '.$nomor.' TIDAK Dalam Pinjaman!';
					echo json_encode($r); 
					die();
			}else if($jumlah==1){

				/* GET transaction detail */			
				$con=array();			 
				$con['id_detail']=$datakatalog[0]['id_detail'];
                $con['status']=1;
                $pak = new Mpinjam_aktif();
                $r=$pak->where($con)->findAll();
                $detail=$r[0];
				if(count($r)>0){
					$this->inventory_start();
					
					/* transaksi buku masuk */
					$val=array();
					$val['katalog_id']=$detail['katalog_id'];
					$val['masuk']=1;
                    $val['keterangan']='PI '.$detail['id_pinjam'];
                    $mi = new Minventory();
                    $rrr=$mi->insert($val);
					
					$id=$mi->getInsertID();;


					/* UPDATE PINJAMAN DETAIL */
					$con=array();
					$con['id_detail']=$detail['id_detail'];					
					$con['inventory_masuk_id']=$id;
					$con['status']=2;
					$con['tanggal_kembali']=date("Y-m-d H:i:s");
                    $con['admin_kembali_id']=session()->get('UID');	
                    $pd = new Mpinjaman_detail();
					$pd->save($con);
					$this->inventory_end();
				}
				
				/* CHECK PINJAMAN TERSISA */
				$c=array();
				$c['pinjam_id']=$detail['id_pinjam'];
                $c['status!=']=2;
                $pdx = new Mpinjaman_detail();
				$pinjaman=$pdx->where($c)->findAll();

				if(count($pinjaman)==0){
					/* UPDATE STATUS PINJAMAN */
					$cond=array();
					$cond['id_pinjam']=$detail['id_pinjam'];
                    $cond['status_pinjam']=2;
                    $pi = new Mpinjaman();
					$pi->save($cond);
				}


				if(isset($id)){
					$r=array();
					$r['status']=1;
					$r['msg']='Berhasil! Nomor '.$nomor.' Diupdate';
					echo json_encode($r); 
					die();
				}else{
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! ADA YANG SALAH';
					echo json_encode($r); 
					die();
				}
			}else{
                /* jika ditemukan lebih dari 1 pinjaman */
                    $return =array();
                    foreach ($datakatalog as $d) {
                        
                        $mm = new Mmember();
                        $member = $mm->find($d['member_id']);
                        $d['nama']= $member['nama'];
                        $d['tanggal'] = date('d/m/Y',strtotime($d['tanggal_pinjam']));
                        $d['pid'] = date('Ym',strtotime($d['created_at'])).sprintf('%04d',$d['id_pinjam']);
                        $return[]= $d;
                    }
                    
					$r=array();
					$r['status']=2;
					$r['msg']='Pilih Nomor Pinjaman';
					$r['pinjaman']=$return;
					echo json_encode($r); 
					die();
			}	
			 
		}else{
			$r=array();
			$r['status']=0;
			$r['msg']='GAGAL! ID Tidak Ditemukan';
			echo json_encode($r);
			die();
		}
    }

    public function pengembalian_submit(){

        if( $this->request->getPost('id_detail') !== NULL ){			
        $iddetail = $this->request->getPost('id_detail');
     
        /* GET transaction detail */			
				$con=array();			 
				$con['id_detail']=$iddetail;
                $con['status']=1;
                $pak = new Mpinjam_aktif();
                $r=$pak->where($con)->findAll();
                $detail=$r[0];
				if(count($r)>0){
					$this->inventory_start();
					
					/* transaksi buku masuk */
					$val=array();
					$val['katalog_id']=$detail['katalog_id'];
					$val['masuk']=1;
                    $val['keterangan']='PI '.$detail['id_pinjam'];
                    $mi = new Minventory();
                    $rrr=$mi->insert($val);					
					$id=$mi->getInsertID();;


					/* UPDATE PINJAMAN DETAIL */
					$con=array();
					$con['id_detail']=$detail['id_detail'];					
					$con['inventory_masuk_id']=$id;
					$con['status']=2;
					$con['tanggal_kembali']=date("Y-m-d H:i:s");
                    $con['admin_kembali_id']=session()->get('UID');	
                    $pd = new Mpinjaman_detail();
					$pd->save($con);
					$this->inventory_end();
				}
				
				/* CHECK PINJAMAN TERSISA */
				$c=array();
				$c['pinjam_id']=$detail['id_pinjam'];
                $c['status!=']=2;
                $pdx = new Mpinjaman_detail();
				$pinjaman=$pdx->where($c)->findAll();

				if(count($pinjaman)==0){
					/* UPDATE STATUS PINJAMAN */
					$cond=array();
					$cond['id_pinjam']=$detail['id_pinjam'];
                    $cond['status_pinjam']=2;
                    $pi = new Mpinjaman();
					$pi->save($cond);
				}


				if(isset($id)){
					$r=array();
					$r['status']=1;
					$r['msg']='Berhasil! Pinjaman Diupdate';
					echo json_encode($r); 
					die();
				}else{
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! ADA YANG SALAH';
					echo json_encode($r); 
					die();
                }
        }else{
			$r=array();
			$r['status']=0;
			$r['msg']='GAGAL! ID Tidak Ditemukan';
			echo json_encode($r);
			die();
		}
    }


    public function pinjaman_buku_list(){
        $data['title']= 'Daftar Pinjaman Aktif';
        return view('admin_pinjaman_aktif',$data);
    }
    public function pinjaman_buku_list_ajax(){
        $db = db_connect();
        $builder = $db->table('pinjam_aktif')
                        ->select('concat(date_format(pinjam_aktif.created_at,"%Y%m"),LPAD(id_pinjam,4,0)) as pid,
                                date_format(tanggal_pinjam,"%d-%m-%Y") as tp,
                                judul,
                                pengarang,
                                concat_ws(".",SUBSTRING(bahasa,1,1),YEAR(katalog.created_at),id_katalog) as noreg,
                                nama,
                                telp')                         
                        ->join('katalog', 'katalog_id=id_katalog')
                       
                        ->join('member', 'member_id=id_member');
        return DataTable::of($builder)                
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }


    public function status_pinjaman($id){
        
		if($id==null)
            die();
            
        /* GET USER */        
        $pa = new Mpinjam_aktif();
		$pinjaman=$pa->where(array('member_id'=>$id))->findAll();
		$result=array();
		if(count($pinjaman)>0){
            /* INSERT KUNJUNGAN */

			$result['status']=0;
			$result['result']=$pinjaman[0];
			 
		}else{
            $like=array();
            $like['identitas'] = $id;	
            $pa = new Mpinjam_aktif();
            $result2=$pa->where($like)                               
                ->join('member','member_id=id_member')
                ->findAll();

            if(count($result2)>0){
                $result['status']=0;
			    $result['result']=$result2[0];
            }else{
                $result['status']=1;
			    $result['result']=array();;
            }            
			
		}
		header('Content-Type: application/json');
		
		echo  json_encode($result);
    }



    /* MEMBER AND ADMIN */

    public function user_admin(){

        
        $data['title']= 'Daftar User Admin';
        return view('admin_user_admin',$data);
    }

    public function user_admin_ajax(){
        $db = db_connect();
        $builder = $db->table('admin')
                        ->select('id_admin,nama,username,email,password,akses,status')
                        ->where('admin.deleted_at IS NULL');
         
        return DataTable::of($builder)   
                ->add('action', function($row){
                    $text = '<a href="javascript:void(0);" class="edit_record btn btn-transparent btn-sm text-primary" data-id="'.$row->id_admin.'" data-nama="'.$row->nama.'" data-username="'.$row->username.'" data-email="'.$row->email.'" data-akses="'.$row->akses.'" data-status="'.$row->status.'" title="Edit User"><i class="far fa-edit"></i></a> ';

                    if($row->id_admin == 1 || $row->id_admin == session()->get('UID') ){

                    }else
                        $text .='<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger" data-id="'.$row->id_admin.'" title="Hapus User"><i class="fas fa-trash"></i></a>';
                    if(session()->get('akses') == 1)											   
                        $text .='<button class="btn btn-transparent btn-sm text-info" title="Lihat Password" onclick="alert(\''.$row->password.'\')"><i class="fas fa-eye"></i></button>';
                    return $text;
                }, 'last')
                ->edit('status', function($row){
                    if($row->status==0)
                        return '<a href="#" class=" btn-transparent btn-sm text-danger"> <i class="fas fa-ban" title="Tidak Aktif"></i></a>';
                    else if($row->status==1)
                        return '<a href="#" class=" btn-transparent btn-sm text-success"><i class="far fa-check-circle" title="Aktif"></i></a>';
                    else
                        return 'N/A';
                })
                ->edit('akses', function($row){
                    if($row->akses==1)
                        return 'Admin';                    
                    else
                        return 'Operator';
                })
               ->hide('id_admin')
               ->hide('password')
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }


    public function admin_username_check(){
        if( $this->request->getPost('username') !== NULL){
			$con=array();
            $con['username']=$this->request->getPost('username');
            $u= new Muser();
            $user = $u->where($con)->findAll();
			
			if(count($user)>0){
				echo '0';
			}else{
				echo '1';
			}
		}else{
			echo '0';
		}
    }

    public function admin_user_add(){

        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'nama' => 'required',
                                'username' => 'required',                                
                                'akses' => 'numeric'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $con=array();
            $con['id_admin']=$this->request->getPost('id');
            $con['nama']=$this->request->getPost('nama');
            $con['username']=$this->request->getPost('username');
            if($this->request->getPost('id') == '')
                $con['password']=$this->request->getPost('password');
            else if($this->request->getPost('password')!='')
                $con['password']=$this->request->getPost('password');
            $con['email']=$this->request->getPost('email');
            $con['akses']=$this->request->getPost('akses');
            if($this->request->getPost('status')!==null){
                $con['status']=$this->request->getPost('status');
            }else{
                if($con['id_admin']!=1)
                $con['status']=0;
            }

             
            $u= new Muser();
            $result= $u->save($con);
                if($result){
                    if($this->request->getPost('id') == ''){
                        $id=$u->getInsertID();
                        //echo 'valid';
                        $session->setFlashdata('error', 'Data berhasil disimpan');         
                    }else{                         
                         $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                    }                    
                }else{
                    $session->setFlashdata('error', 'Update Data GAGAL');  
                }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Update Admin GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/admin_list');
    }
    public function admin_user_delete(){
        if( $this->request->getPost('id') !== NULL){		
            $session = session();	 
            $m=new Muser();
			$r=$m->delete($this->request->getPost('id'));			
			if($r){
                $session->setFlashdata('pesan', 'user berhasil dihapus');
			}	else{
                $session->setFlashdata('pesan', 'user GAGAL dihapus');
			}
		}

			
        
		return redirect()->to('panel/admin_list');
		 
    }

    public function user_member(){

        
        $data['title']= 'Daftar Member';
        $prov = new Mprovinsi();
        $data['provinsi'] = $prov->findall();
        return view('admin_user_member',$data);
    }

    public function user_member_ajax(){
        $db = db_connect();
        $builder = $db->table('member')
                        ->select('id_member,nama,identitas,alamat,dis_name,city_name,prov_name,kodepos,telp,member.status,password')
                        ->where('member.deleted_at IS NULL')
                        ->join('provinces','prov_id=provinsi')
                        ->join('cities','city_id=kota')
                        ->join('districts','dis_id=kec');
         
        return DataTable::of($builder)   
                ->add('action', function($row){
                    $text = '<a href="javascript:void(0);" class="edit_record btn btn-transparent btn-sm text-primary" data-id="'.$row->id_member.'" title="Edit User"><i class="far fa-edit"></i></a> ';
                    $text .='<a href="javascript:void(0);" class="btn btn-transparent btn-sm text-info" title="Lihat Password" onclick="alert(\''.$row->password.'\')"><i class="fas fa-eye"></i></a>';
                    $text .='<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger" data-id="'.$row->id_member.'" title="Hapus User"><i class="fas fa-trash"></i></a>';
                    
                    return $text;
                }, 'last')  
                ->edit('type', function($row){
                    if($row->type==1)
                        return 'member';
                    else if($row->type==2)
                        return 'Siswa';
                    else
                        return 'Guru';
                })
                ->edit('status', function($row){
                    if($row->status==0)
                        return '<span class=" btn-transparent btn-sm text-danger"> <i class="fas fa-ban" title="Tidak Aktif"></i></span>';
                    else if($row->status==1)
                        return '<span class=" btn-transparent btn-sm text-success"><i class="far fa-check-circle" title="Aktif"></i></span>';
                    else
                        return 'N/A';
                }) 
                ->hide('password')               
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }


    public function member_username_check(){
        if( $this->request->getPost('identitas') !== NULL){
			$con=array();
            $con['identitas']=$this->request->getPost('identitas');
            $u= new Mmember();
            $user = $u->where($con)->findAll();
			
			if(count($user)>0){
				echo '0';
			}else{
				echo '1';
			}
		}else{
			echo '0';
		}
    }
    public function member_email_check(){
        if( $this->request->getPost('email') !== NULL){
			$con=array();
            $con['email']=$this->request->getPost('email');
            $u= new Mmember();
            $user = $u->where($con)->findAll();
			
			if(count($user)>0){
				echo '0';
			}else{
				echo '1';
			}
		}else{
			echo '0';
		}
    }

    public function member_user_add(){

        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'identitas' => 'required',
                                'nama' => 'required'                              
                                 
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $con=array();
            $con['identitas']=$this->request->getPost('identitas');
            $m = new Mmember();
            $x = $m->where($con)->findAll();

            if($this->request->getPost('id') == '' && count($x)>0){
                $session->setFlashdata('error', 'Identitas Sudah Dipakai');  
            }else{
                $value=array();
				$value['id_member']=$this->request->getPost('id');
				$value['nama']=$this->request->getPost('nama');
				$value['identitas']=$this->request->getPost('identitas');
				$value['alamat']=$this->request->getPost('alamat');
				$value['desa']=$this->request->getPost('desa');
				$value['kec']=$this->request->getPost('kec');
				$value['kota']=$this->request->getPost('kota');
				$value['provinsi']=$this->request->getPost('provinsi');
				$value['kodepos']=$this->request->getPost('kodepos');
				$value['telp']=$this->request->getPost('telp');
                $value['email']=$this->request->getPost('email');	
                if($this->request->getPost('password')!='')			
				$value['password']=$this->request->getPost('password');				
                $value['type']=$this->request->getPost('tipe');
                if($this->request->getPost('status')!==null){
					$value['status']=$this->request->getPost('status');
				}else{
					$value['status']=0;
                }	
                $m = new Mmember();
                $result = $m->save($value);
                if($result){
                    if($this->request->getPost('id') == ''){
                        $id=$m->getInsertID();
                        //echo 'valid';
                        $session->setFlashdata('error', 'Data berhasil disimpan');         
                    }else{                         
                         $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                    }                    
                }else{
                    $session->setFlashdata('error', 'Update Data GAGAL');  
                }
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Update Admin GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/member_list');
    }

    public function member_user_json($id){
        
		if($id==null)
		{
            $result['status']=0;
            $result['result']=array();;
            die();
        }	
        $m = new Mmember();
		$user=$m->find($id);
		$result=array();
		if($user!=''){
			$result['status']=1;
			$result['result']=$user;
		}else{
			$result['status']=0;
			$result['result']=array();;
		}
		header('Content-Type: application/json');
		
		echo  json_encode($result);
    }

    public function member_user_delete(){
        if( $this->request->getPost('id') !== NULL){		
            $session = session();	 
            $m=new Mmember();
			$r=$m->delete($this->request->getPost('id'));			
			if($r){
                $session->setFlashdata('pesan', 'user berhasil dihapus');
			}	else{
                $session->setFlashdata('pesan', 'user GAGAL dihapus');
			}
		}

			

		return redirect()->to('panel/member_list');
		 
    }
    public function app_kota(){
        $result=array();		
		if($this->request->getpost('prov_id')!==null){
			
			$con=array();
			$con['prov_id']=$this->request->getpost('prov_id');
            $ax=new Mkota();
           
            $result=$ax->where($con)->findAll();
            
		}
		echo json_encode($result);
    }
    public function app_kecamatan(){
        $result=array();		
		if($this->request->getpost('city_id')!==null){
			
			$con=array();
			$con['city_id']=$this->request->getpost('city_id');
			$ax=new MKec();
			$result=$ax->where($con)->findAll();
		}
		echo json_encode($result);
    }
    public function app_desa(){
        $result=array();		
		if($this->request->getpost('dis_id')!==null){
			
			$con=array();
			$con['dis_id']=$this->request->getpost('dis_id');
			$ax=new Mdesa();
			$result=$ax->where($con)->findAll();
		}
		echo json_encode($result);
    }

    /* END OF MEMBER ADMIN */


    public function cari_member(){
        $con=$this->request->getGet('term');
		if($con==''){
			echo json_encode(array());
			die();
		}
		$like=array();
		
		$like['nama'] = $con;		
        $cs= new Mmember();    
        $r = $cs->select('id_member,
                         nama,telp')->like($like)
                         ->where('deleted_at IS null')
                        ->findAll(10);
       
                $result=array();
                foreach($r as $rr){
                    $temp=array();
                    
                    $temp=array('value'=>$rr['id_member'], 'label'=>$rr['nama'].'| '.$rr['telp']);
                    $result[]=$temp;
                };
                

        header('Content-Type: application/json');
		echo json_encode($result);
    }



    public function setting(){
    
            $data['title']='Pengaturan Aplikasi';
            $this->admintemplate->load('admin_setting',$data);
       
    }
}
