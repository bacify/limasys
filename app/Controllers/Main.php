<?php

namespace App\Controllers;
use App\Models\Mkatalog; 
use App\Models\Mkatalogstok; 
use App\Models\Mmember; 
use App\Models\Mkunjungan; 
use App\Models\Mpinjam_aktif; 
 

class Main extends BaseController
{
	public function index()
	{
        
        if($this->request->getVar('kata_kunci')){
            $katakunci=$this->request->getVar('kata_kunci');
            redirect()->to('/cari/'.urlencode($katakunci));            
        }     
		$data['title']='Limasys';
		return view('home',$data);
    }
    public function home(){
        if($this->request->getVar('kata_kunci')){
            echo 'aaa';
            $katakunci=$this->request->getVar('kata_kunci');
            return redirect()->to('/cari/'.urlencode($katakunci));            
        }     
		$data['title']='Limasys';
		return view('home',$data);
    }
    
    public function cari($katakunci){
         
        
        $data['katakunci']=$katakunci;
        $like=array();
		$like['no_register'] = $katakunci;
		$like['no_panggil'] = $katakunci;
		$like['judul'] = $katakunci;
		$like['pengarang'] = $katakunci;
		$like['subyek'] = $katakunci;
            $cs= new Mkatalog();    
            $result = $cs->orLike($like)
                    ->findAll();
            $ret=array();
            if(count($result) > 0){
                foreach($result as $r){

                    /* get all katakunci */
                    $r['no_register'] = str_replace( strtolower($katakunci) , '<b>'.strtolower($katakunci).'</b>' , strtolower($r['no_register']));
                    $r['no_panggil'] = str_replace( strtolower($katakunci) , '<b>'.strtolower($katakunci).'</b>' , strtolower($r['no_panggil']));
                    $r['judul'] = str_replace( strtolower($katakunci) , '<b>'.strtolower($katakunci).'</b>' , strtolower($r['judul']));
                    $r['pengarang'] = str_replace( strtolower($katakunci) , '<b>'.strtolower($katakunci).'</b>' , strtolower($r['pengarang']));
                    $r['subyek'] = str_replace( strtolower($katakunci) , '<b>'.strtolower($katakunci).'</b>' , strtolower($r['subyek']));


                    /* GET STATUS */
                    $x=array();
                    $con=array();
                    $con['katalog_id']=$r['id_katalog'];
                    $ks = new Mkatalogstok();
                    $x=$ks->where($con)->first();
                    if(count($x)>0){
                        if($x['stok']> 0){
                            $r['status']=1;
                        }else{
                            $r['status']=0;
                        }
                    }else{
                        $r['status']=0;
                    }

                    $ret[] =$r;
                }
            }
             
            $data['hasil']=$ret;
            return view('hasil_cari',$data);
    } 

    public function kunjungan(){
        $data['title']= 'Daftar Hadir';
        return view('kunjungan',$data);
    }
    public function kunjungan_add($id){		 
		 
		if($id==null)
            {$result['status']=0;
            $result['result']=array();
        }else{   
            /* GET USER */  
            $m = new Mmember();
            $con=array();
            $con['id_member']=$id;
            $con['identitas']=$id;
            $con['status']=1;
		    $user=$m->orWhere($con)->first();
            $result=array();
            if($user!=''){
                /* INSERT KUNJUNGAN */

                $val=array();
                $val['member_id']=$id;
                $k = new Mkunjungan();
                $r=$k->insert($val);
                if($r){
                    $result['status']=1;
                    $result['result']=$user;
                }else{
                    $result['status']=0;
                    $result['result']=array();;
                }
                
            }else{
                 
                $result['status']=0;
                $result['result']=array();;
            }
        }
		header('Content-Type: application/json');		
		echo  json_encode($result);
    }
    
    public function cek_pinjaman(){
        if($this->request->getPost('nomor_anggota')){
            $katakunci=$this->request->getPost('nomor_anggota');
            return redirect()->to('cek_pinjaman/'.urlencode($katakunci));
        }    
        $data['title'] = 'Cek Pinjaman Member';
         
        return view('cek_pinjam',$data);
    }

    public function cek_pinjaman_member($katakunci){
         
        
         
        
        $data['katakunci']=$katakunci;                
        $like=array();
		$like['member_id'] = $katakunci;	

        $pa = new Mpinjam_aktif();
        $result=$pa->where($like)
                ->select('judul,pengarang,penerbit,no_panggil,tahun,concat(date_format(pinjam_aktif.created_at,"%Y%m"),LPAD(id_pinjam,4,0)) as pid,tanggal_pinjam')
                ->join('katalog','katalog_id=id_katalog')                
                ->findAll();
        
        if(count($result)>0){
            $data['hasil']=$result;
            $data['jenis']=1;
        }else{
            $like=array();
            $like['identitas'] = $katakunci;	
            $pa = new Mpinjam_aktif();
            $result2=$pa->where($like)
                ->select('judul,pengarang,penerbit,no_panggil,tahun,concat(date_format(pinjam_aktif.created_at,"%Y%m"),LPAD(id_pinjam,4,0)) as pid,tanggal_pinjam')
                ->join('katalog','katalog_id=id_katalog')
                ->join('member','member_id=id_member')
                ->findAll();
            $data['hasil']= $result2;            
            $data['jenis']=2;
        }
        
         return view('hasil_cek_pinjam',$data);
    } 


    public function new_koleksi(){

        $ks = new Mkatalog();

        $result=$ks->orderBy('id_katalog', 'desc')->findAll(50);
        $data['hasil']=$result;
        return view('koleksi_baru',$data);
    }


    public function informasi(){
        return view('informasi');
    }
}
