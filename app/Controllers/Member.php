<?php

namespace App\Controllers;
use App\Models\Mkatalog; 

class Home extends BaseController
{
	public function index()
	{
        $con=array();
		$con['status_pinjam']=1;
		$con['member_id']=$this->session->userdata('UID');
		$con['peminjaman_detail.status']=1;
		$tanggungan=$this->bookman_model->get_pinjaman_with_detail($con);
		$data['tanggungan']=count($tanggungan);
		
		/* GET KUNJUNGAN */
		$con=array();
		$con['member_id']=$this->session->userdata('UID');
		$jumlahkunjungan=$this->bookman_model->get_jumlah_kunjungan($con);
		$data['kunjungan']=$jumlahkunjungan[0]->jumlah;

		/* GET PINJAMAN ONLINE */
		$con=array();
		$con['member_id']=$this->session->userdata('UID');
		$pinjaman_online=$this->bookman_model->get_booking($con);
		$data['booking_online']=count($pinjaman_online);
        return view('m_home');
	}
}
