<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use App\Models\Mmember; 
use App\Models\Muser; 
 

class Login extends BaseController
{
	public function index()
    {
        
        helper(['form']);
        echo view('c_login_page');
    } 
    public function auth()
    {
        
        $session = session();
       
        $username = $this->request->getVar('username');
        $password = $this->request->getVar('password');
        $m = new Mmember();
        $d= $m->where('email',$username)->first();
         
        
        
        if($d){
            $pass = $d['password'];
            $verify_pass = ($password === $pass);
            if($verify_pass){
                $ses_data = [
                    'UID' => $d['id_member'],
                    'id_member'       => $d['id_member'],
                    'nama'     => $d['nama'],                    
                    'email'     => $d['email'],                    
                    'type'     => 'member',                    
                    'logged_in'     => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/member');
            }else{
                $session->setFlashdata('error', 'Wrong Password');                
                return redirect()->to('/login');
            }
        }else{
            
            $session->setFlashdata('error', 'Username not Found');
            
            return redirect()->to('/login');
        }               

    }

    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('/login');
    }

    public function admin()
    {
        if(session()->get('logged_in')){
            // maka redirct ke halaman login
            return redirect()->to('/panel/home'); 
        }
        helper(['form']);
        echo view('admin_login');
    } 
    public function admin_auth()
    {
        
        $session = session();
       
        $username = $this->request->getVar('username');
        $password = $this->request->getVar('password');
        $m = new Muser();
        $d= $m->where('username',$username)->where('status',1)->first();
         
        
        
        if($d){
            $pass = $d['password'];
            $verify_pass = ($password === $pass);
            if($verify_pass){
                $ses_data = [
                    'UID' => $d['id_admin'],
                    'id_admin'       => $d['id_admin'],
                    'nama'     => $d['nama'],                    
                    'username'     => $d['username'],                    
                    'email'     => $d['email'],                    
                    'akses'     => $d['akses'],                    
                    'type'     => 'admin',                    
                    'logged_in'     => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/panel/home');
            }else{
                $session->setFlashdata('error', 'Wrong Password');                
                return redirect()->to('/panel');
            }
        }else{
            
            $session->setFlashdata('error', 'Username not Found');
            
            return redirect()->to('/panel');
        }               

    }
 
    public function admin_logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('/panel');
    }

}