
<?php $this->extend('admin/page_layout'); ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Master Katalog</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Master Katalog</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">
        
      <div class="container-fluid">
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Database katalog</h3>
                <button class="btn btn-success float-right" data-toggle="modal" data-target="#myModalAdd">Tambah Baru</button>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-sm table-bordered table-sm" width="100%">
                <thead>
                        <tr>
                        <th>No</th>
                        <th class="text-nowrap">no register</th>
                        <th class="text-nowrap">no panggil</th>
                        <th>judul</th>
                        <th>pengarang</th>
                        <th>penerbit</th>                        
                        <th>kategori</th>
                        <th >Tindakan</th>
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->



         <!-- Modal Add Product-->
         <form id="form-tambah-buku" action="<?php echo base_url('panel/new_katalog');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Tambah/Update katalog Baru</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <!-- <div class="form-group">
                        
                        <label for="no_register" class="mb-0 pb-0">Nomor Register</label>
                           
                           <input type="text" name="no_register" class="form-control" placeholder="Nomor Register">
                       </div> -->
                       <div class="form-group">
                            <label for="judul" class="mb-0 pb-0">Judul</label>
                            <input type="hidden" name="id" value="" > 
                           <input type="text" name="judul" class="form-control" placeholder="Judul Buku" required>
                       </div>
                       <div class="form-group">
                            <label for="no_panggil" class="mb-0 pb-0">No Panggil</label>
                           <input type="text" name="no_panggil" class="form-control" placeholder="Nomor Panggil (ex: '793.7 Woe k')" >
                       </div>
                        <div class="form-group">
                            <label for="pengarang" class="mb-0 pb-0">pengarang</label>
                           <input type="text" name="pengarang" class="form-control" placeholder="Pengarang" required>
                       </div>
                        <div class="form-group">
                            <label for="penerbit" class="mb-0 pb-0">Penerbit</label>
                           <input type="text" name="penerbit" class="form-control" placeholder="penerbit" required>
                       </div>
                       <div class="form-group">
                        <label for="kategori" class="mb-0 pb-0">Kategori</label>
                              
                              
                              <select name="kategori" class="form-control" data-live-search="true" title="Jenis Katalog" required>
                                    <?php foreach ($kategori as $row) :?>
                                    <option value="<?php echo $row['id_kategori'];?>"><?php echo $row['nama_kategori'];?></option>
                                    <?php endforeach;?>
                            </select>
                       </div>
                       <div class="form-group">
                        <label for="bahasa" class="mb-0 pb-0">Bahasa</label>                         
                                <select name="bahasa" class="form-control" data-live-search="true" title="Bahasa" >                                
                                                        <option value="Indonesia">Indonesia</option>
                                                        <option value="Asing">Asing</option>                                                        
                                </select>                           
                       </div>
                        <div class="form-group">
                        <label for="tahun" class="mb-0 pb-0">Tahun</label>
                           <input type="text" name="tahun" class="form-control" placeholder="tahun" required>
                       </div>
                        <div class="form-group">
                        <label for="edisi" class="mb-0 pb-0">Edisi</label>
                           <input type="text" name="edisi" class="form-control" placeholder="Edisi" required>
                       </div>
                        <div class="form-group">
                        <label for="subyek" class="mb-0 pb-0">Subyek</label>
                           <input type="text" name="subyek" class="form-control" placeholder="Subyek" required>
                       </div>
                        <div class="form-group">
                        <label for="klasifikasi" class="mb-0 pb-0">Klasifikasi</label>
                           <input type="text" name="klasifikasi" class="form-control" placeholder="klasifikasi (ex: 793.7)" >
                       </div>
                        <div class="form-group">
                        <label for="deskripsi" class="mb-0 pb-0">Deskripsi</label>
                           <input type="text" name="deskripsi" class="form-control" placeholder="Deskripsi Fisik (ex: 'xii, 200 hal. : ill. ; 18 cm)'" >
                       </div>
                        <div class="form-group">
                            <label for="isbn" class="mb-0 pb-0">isbn</label>
                           <input type="text" name="isbn" class="form-control" placeholder="isbn" >
                       </div>
                                         
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
 <!-- Modal View Product-->
<form id="viewform" class="form-horizontal" action="<?php echo base_url('menu/master_buku_update');?>" method="post">

         <div class="modal fade" id="ModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content ">
                   <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Detail Katalog</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <div class="form-group row">
                        <label for="no_register" class="col-md-2  control-label my-auto py-auto">Nomor Register</label>
                            <div class="col-md-10">
                            <input type="hidden" name="id" value="0" class="form-control">
                           <input type="text" name="no_register" class="form-control" placeholder="Nomor Register" disabled>
                           </div>
                       </div>
                       <div class="form-group row">
                            <label for="judul" class="col-md-2  control-label my-auto py-auto">Judul</label>
                            <div class="col-md-10">
                           <input type="text" name="judul" class="form-control" placeholder="Judul Buku" disabled>
                           </div>
                       </div>
                       <div class="form-group row">
                            <label for="no_panggil" class="col-md-2  control-label my-auto py-auto">No Panggil</label>
                            <div class="col-md-10">
                           <input type="text" name="no_panggil" class="form-control" placeholder="Nomor Panggil (ex: '793.7 Woe k')" disabled>
                           </div>
                       </div>
                        <div class="form-group row">
                            <label for="pengarang" class="col-md-2  control-label my-auto py-auto">pengarang</label>
                            <div class="col-md-10">
                           <input type="text" name="pengarang" class="form-control" placeholder="Pengarang" disabled>
                           </div>
                       </div>
                        <div class="form-group row">
                            <label for="penerbit" class="col-md-2  control-label my-auto py-auto">Penerbit</label>
                            <div class="col-md-10">
                           <input type="text" name="penerbit" class="form-control" placeholder="penerbit" disabled>
                           </div>
                       </div>
                       <div class="form-group row">
                        <label for="kategori" class="col-md-2  control-label my-auto py-auto">Kategori</label>
                        <div class="col-md-10">
                           <select name="kategori" class="form-control" data-live-search="true" title="Jenis Katalog" disabled>
                                         
                                                      <?php foreach ($kategori as $row) :?>
                                                        <option value="<?php echo $row['id_kategori'];?>"><?php echo $row['nama_kategori'];?></option>
                                                        <?php endforeach;?>
                                                 </select>
                                                 </div>
                       </div>
                        <div class="form-group row">
                        <label for="bahasa" class="col-md-2  control-label my-auto py-auto">Bahasa</label>
                        <div class="col-md-10">
                                <select name="bahasa" class="form-control" data-live-search="true" title="Bahasa" disabled>
                                
                                                        <option value="Indonesia">Indonesia</option>
                                                        <option value="Asing">Asing</option>
                                                        
                                                 </select>
                           </div>
                       </div>
                        <div class="form-group row">
                        <label for="tahun" class="col-md-2  control-label my-auto py-auto">Tahun</label>
                        <div class="col-md-10">
                           <input type="text" name="tahun" class="form-control" placeholder="tahun" disabled>
                           </div>
                       </div>
                        <div class="form-group row">
                        <label for="edisi" class="col-md-2  control-label my-auto py-auto">Edisi</label>
                        <div class="col-md-10">
                           <input type="text" name="edisi" class="form-control" placeholder="Edisi" disabled>
                           </div>
                       </div>
                        <div class="form-group row">
                        <label for="subyek" class="col-md-2  control-label my-auto py-auto">Subyek</label>
                        <div class="col-md-10">
                           <input type="text" name="subyek" class="form-control" placeholder="Subyek" disabled>
                           </div>
                       </div>
                        <div class="form-group row">
                        <label for="klasifikasi" class="col-md-2  control-label my-auto py-auto">Klasifikasi</label>
                        <div class="col-md-10">
                           <input type="text" name="klasifikasi" class="form-control" placeholder="klasifikasi (ex: 793.7)" disabled>
                           </div>
                       </div>
                        <div class="form-group row">
                        <label for="deskripsi" class="col-md-2  control-label my-auto py-auto">Deskripsi</label>
                        <div class="col-md-10">
                           <input type="text" name="deskripsi" class="form-control" placeholder="Deskripsi Fisik (ex: 'xii, 200 hal. : ill. ; 18 cm)'"disabled >
                           </div>
                       </div>
                        <div class="form-group row">
                            <label for="isbn" class="col-md-2  control-label my-auto py-auto">isbn</label>
                            <div class="col-md-10">
                           <input type="text" name="isbn" class="form-control" placeholder="isbn" disabled>
                           </div>
                       </div>
                                         
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
                         
                   </div>
                    </div>
            </div>
         </div>
     </form>

<!-- Modal Update Product-->
<form id="updateform" action="<?php echo base_url('panel/new_katalog');?>" method="post">
         <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content">
                   <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Update Katalog</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                        <label for="no_register" class="mb-0 pb-0">Nomor Register</label>
                            <input type="hidden" name="id" value="0" class="form-control">
                           <input type="text" name="no_register" class="form-control" placeholder="Nomor Register">
                       </div>
                       <div class="form-group">
                            <label for="judul" class="mb-0 pb-0">Judul</label>
                           <input type="text" name="judul" class="form-control" placeholder="Judul Buku" required>
                       </div>
                       <div class="form-group">
                            <label for="no_panggil" class="mb-0 pb-0">No Panggil</label>
                           <input type="text" name="no_panggil" class="form-control" placeholder="Nomor Panggil (ex: '793.7 Woe k')" >
                       </div>
                        <div class="form-group">
                            <label for="pengarang" class="mb-0 pb-0">pengarang</label>
                           <input type="text" name="pengarang" class="form-control" placeholder="Pengarang" required>
                       </div>
                        <div class="form-group">
                            <label for="penerbit" class="mb-0 pb-0">Penerbit</label>
                           <input type="text" name="penerbit" class="form-control" placeholder="penerbit" required>
                       </div>
                       <div class="form-group">
                        <label for="kategori" class="mb-0 pb-0">Kategori</label>
                           <select name="kategori" class="form-control" data-live-search="true" title="Jenis Katalog" required>
                                    <?php foreach ($kategori as $row) :?>
                                    <option value="<?php echo $row['id_kategori'];?>"><?php echo $row['nama_kategori'];?></option>
                                    <?php endforeach;?>
                            </select>
                       </div>
                       <div class="form-group">
                        <label for="bahasa" class="mb-0 pb-0">Bahasa</label>                         
                                <select name="bahasa" class="form-control" data-live-search="true" title="Bahasa" required>                                
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Asing">Asing</option>                                                        
                                </select>                           
                       </div>
                        <div class="form-group">
                        <label for="tahun" class="mb-0 pb-0">Tahun</label>
                           <input type="text" name="tahun" class="form-control" placeholder="tahun" required>
                       </div>
                        <div class="form-group">
                        <label for="edisi" class="mb-0 pb-0">Edisi</label>
                           <input type="text" name="edisi" class="form-control" placeholder="Edisi" required>
                       </div>
                        <div class="form-group">
                        <label for="subyek" class="mb-0 pb-0">Subyek</label>
                           <input type="text" name="subyek" class="form-control" placeholder="Subyek" required>
                       </div>
                        <div class="form-group">
                        <label for="klasifikasi" class="mb-0 pb-0">Klasifikasi</label>
                           <input type="text" name="klasifikasi" class="form-control" placeholder="klasifikasi (ex: 793.7)" >
                       </div>
                        <div class="form-group">
                        <label for="deskripsi" class="mb-0 pb-0">Deskripsi</label>
                           <input type="text" name="deskripsi" class="form-control" placeholder="Deskripsi Fisik (ex: 'xii, 200 hal. : ill. ; 18 cm)'" >
                       </div>
                        <div class="form-group">
                            <label for="isbn" class="mb-0 pb-0">isbn</label>
                           <input type="text" name="isbn" class="form-control" placeholder="isbn" >
                       </div>
                                         
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Update</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
     <!-- Modal delete Product-->
      <form id="deleteform" action="<?php echo base_url('panel/remove_katalog');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Katalog</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  

<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js');?>"></script>
 

<script>
    $(document).ready(function(){
        // selectpicker
        $('select').selectpicker();

        
        $(document).ready(function() {
        table = $('#tabel-master-katalog').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/plugins/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo site_url('panel/katalogajax')?>",
            
            columnDefs: [
                
                { targets: -1, className: 'text-nowrap'}, //last column center.
                { targets: 2, className: 'text-nowrap'}, //last column center.
               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'excel',
                title: 'Katalog <?php echo date('d-m-Y'); ?> ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: 'Katalog <?php echo date('d-m-Y'); ?> ',
                    messageTop :'Katalog <?php echo date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
         ],
        });
    });
    

            $('#tabel-master-katalog').on('click','.view_record',function(){

                    var id=$(this).data('id');
                    $.ajax({
                    dataType: "json",
                    url: '<?php echo base_url();?>/panel/katalogjson/'+id                    
                    }).done(function(data){
                        
                        console.log(data);
                        $('#ModalView').modal('show');
                        $('#viewform [name="id"]').val(data.id_katalog);
                        $('#viewform [name="no_register"]').val(data.no_register);
                        $('#viewform [name="judul"]').val(data.judul);
                        $('#viewform [name="no_panggil"]').val(data.no_panggil);
                        $('#viewform [name="pengarang"]').val(data.pengarang);
                        $('#viewform [name="penerbit"]').val(data.penerbit);
                        $('#viewform [name="kategori"]').selectpicker('val', data.kategori_id);
                        $('#viewform [name="bahasa"]').selectpicker('val',data.bahasa);
                        $('#viewform [name="tahun"]').val(data.tahun);
                        $('#viewform [name="edisi"]').val(data.edisi);
                        $('#viewform [name="subyek"]').val(data.subyek);
                        $('#viewform [name="klasifikasi"]').val(data.klasifikasi);
                        $('#viewform [name="deskripsi"]').val(data.deskripsi);
                        $('#viewform [name="isbn"]').val(data.isbn);
                        
                   
                    });                            
            });


            // get Edit Records
            $('#tabel-master-katalog').on('click','.edit_record',function(){

                var id=$(this).data('id');
                $.ajax({
                dataType: "json",
                url: '<?php echo base_url();?>/panel/katalogjson/'+id,                
                success:function(data) { 
                    console.log(data);
                    $('#ModalUpdate').modal('show');
                    $('#updateform [name="id"]').val(data.id_katalog);
                    $('#updateform [name="no_register"]').val(data.bahasa.substring(0, 1)+'.'+data.created_at.substring(0, 4)+'.'+data.id_katalog);
                    $('#updateform [name="judul"]').val(data.judul);
                    $('#updateform [name="no_panggil"]').val(data.no_panggil);
                    $('#updateform [name="pengarang"]').val(data.pengarang);
                    $('#updateform [name="penerbit"]').val(data.penerbit);
                    $('#updateform [name="kategori"]').selectpicker('val', data.kategori_id);
                    $('#updateform [name="bahasa"]').selectpicker('val',data.bahasa);
                    $('#updateform [name="tahun"]').val(data.tahun);
                    $('#updateform [name="edisi"]').val(data.edisi);
                    $('#updateform [name="subyek"]').val(data.subyek);
                    $('#updateform [name="klasifikasi"]').val(data.klasifikasi);
                    $('#updateform [name="deskripsi"]').val(data.deskripsi);
                    $('#updateform [name="isbn"]').val(data.isbn);
                    
                }
                });
                 
                    
                        
           
                        
      });
            // End Edit Records
            // get delete Records
            $('#tabel-master-katalog').on('click','.delete_record',function(){
            var id=$(this).data('id');
            $('#ModalDelete').modal('show');
            $('#deleteform [name="id"]').val(id);
      });
            // End delete Records


            
    });
</script>
<?= $this->endSection() ?>