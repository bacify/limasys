
<?php $this->extend('admin/page_layout'); ?>

<?= $this->section('content') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        <small><a href="<?php echo base_url('panel/home');?>" class="text-decoration-none alert-link"><i class="fas fa-chevron-circle-left"></i> kembali</a></small>
          <h1><?php if(isset($title)) echo $title; ?></h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?php if(isset($title)) echo $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
      
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">

          <!-- FORM PENGEMBALIAN -->
            <div class="card">
            <div class="card-header">
                <h3 class="card-title"><?php if(isset($title)) echo $title; ?></h3>
            </div>
                <div class="card-body">
                    <div class="col-8" >
                    
                        <form id="form-kembali" method="POST" action="#">
                        <div class="form-group row my-0 py-0">
                            <label for="tanggal" class="col-md-2 col-form-label ">Cari</label>   
                            <div class="col-md-2">
                                
                                <select name="searchby" class="form-control form-control-sm searchby">
                                    <option value="isbn">ISBN</option>
                                    <option value="id">ID</option>
                                    <option value="register">NoRegister</option>                                
                                    <option value="panggil">NoPanggil</option>                                
                                </select>               
                                
                            </div>                      
                            <div class="col-md-7">
                            
                            <input type="hidden" name="id_katalog_barcode" class="id_katalog_barcode"  value="0">                         
                            <input type="text" name="kode_barcode" class="form-control barcodesearch form-control-sm" onmouseover="this.focus();" placeholder="Ketik NoBarCode" required >
                            
                            </div>                                              
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-outline-primary  btn-sm"   title="Tambah Data Katalog"  ><i class="fas fa-database" ></i></button> 
                            </div>
                        </div>
                        <div class="form-group row my-0 py-0">
                            <div class="col-md-12">
                                <div class="alert alert-success alert-ok" role="alert" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <span class="text-ok"><strong>Sukses!</strong> Data Ditambahkan!</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="alert alert-danger alert-gagal" role="alert" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <span class="text-gagal"><strong>Gagal!</strong> Data gagal Ditambahkan!</span>
                                </div>
                            </div>
                        </div>
                        </form>
                        
                    </div>
                </div>
                 
            </div>
            <!-- /.FORM PENGEMBALIAN -->

            <!-- FORM Cek Member -->
            <div class="card">
           <!--  <div class="card-header">
                <h3 class="card-title">Form Pengembalian Buku</h3>
            </div> -->
                <div class="card-body">
                    <div class="col-8"   >
                    
                        <form method="POST" action="#">
                        <div class="form-group row my-0 py-0">
                            <label for="tanggal" class="col-md-2 col-form-label ">Cek Member</label>                                                
                            <div class="col-md-6">  
                            <input type="text" name="member" id="noidmember" class="form-control nomember form-control-sm" onmouseover="this.focus();" maxlength="15" placeholder="Ketik No Member /Identitas"  required >
                            
                            </div>                                              
                            <!-- <div class="col-md-1">
                                <button type="submit" class="btn btn-outline-primary  btn-sm"   title="Tambah Data Katalog"  ><i class="fas fa-check" ></i></button> 
                            </div> -->
                        </div>
                         
                        </form>
                        
                    </div>
                </div>
                 
            </div>
            <!-- /.FORM Cek Member PENGEMBALIAN -->


            <div class="card">
              <div class="card-header">                
              
              <h3 class="card-title">Riwayat Pengembalian Buku</h3>
                   
              </div>
             
             
              <!-- /.card-header -->
              <div class="card-body">
              
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                <tr>
                        <th>No</th>   
                        <th>NoPinjam</th>                     
                        <th>Pinjam</th>                        
                        <th>kembali</th>                        
                        <th>Judul</th>
                        <th>pengarang</th>  
                        <th>Member</th>                        
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

 
 <!-- MODAL HASIL -->
 <div class="modal fade " id="modal-result" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <form id="resultsearch" method="POST" action ="#">
            <div class="modal-dialog  ">
               <div class="modal-content ">
                   <div class="modal-header">
                   <h5 class="modal-title text-center" id="exampleModalLongTitle">Pilih Pinjaman</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body text-center">                        
                        <h4 class="text-result"><strong class="text-uppercase text-primary">DITEMUKAN lebih dari 1 Pinjaman</strong></h4>
                        <p class="text-muted text-desc">deskripsi</p>
                        <div class="form-group" id="selectpinjam">
                            <label for="nomorpinjam" class="mb-0 pb-0">KodePinjaman</label>                                                                         
                            <select name="nopinjam" class="form-control" data-live-search="true" title="NoPinjam" required>
                                    
                            </select>
                       </div>    
                   </div>
                   <div class="modal-footer">
                   <button type="submit" class="btn btn-sm btn-primary" id="tbok">OK</button>                     
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">close</button>                     
                  </div>
                    
                </div>    
          </div>
          </form>
    </div>
  


<?= $this->endSection() ?>

<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js');?>"></script>
<script>



// SET IDENTITAS NUMBER ONLY
        setInputFilter(document.getElementById("noidmember"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
        }
        $( "#noidmember" ).keypress(function(e) {
            var value=$(this).val();
            console.log(value);
            if(e.which == 13){
                $('.text-result').html('');
                  $('.text-desc').html('');
                        $.ajax({
                                method: "GET",
                                dataType: "json",
                                url: "<?php echo base_url('panel/member_status');?>/"+value,                        
                                })
                                .done(function( msg ) {

                                    $('#selectpinjam').hide();
                                    $('#tbok').hide();
                                    $('.modal-title').html('Hasil');  
                                if(msg.status=='1'){
                                    

                                    $('#modal-result').modal('show');
                                    $('.text-result').html('<strong class="text-uppercase text-primary">OK!</strong>');
                                    $('.text-desc').html('<strong class="text-danger">*tidak ada tanggungan</strong>');
                                  setTimeout(function(){
                                      $('#modal-result').modal('hide');
                                      $("#noidmember" ).val('');
                                      $("#noidmember" ).focus();
                                    }, 3000);
                                } else{
                                    $('#modal-result').modal('show');
                                  $('.text-result').html('<strong class="text-uppercase text-danger">Masih Ada Tanggungan!</strong>');
                                  $('.text-desc').html('<strong class="text-danger">*member memiliki tanggungan buku</strong>');
                                  setTimeout(function(){
                                      $('#modal-result').modal('hide');
                                      $("#noidmember" ).val('');
                                      $("#noidmember" ).focus();
                                    }, 3000);
                                }


                                    return false;
                                });
                return false;    //<---- Add this line
                }
            });


    let table;
    // form submit
    $( "#resultsearch" ).submit(function( event ) {
    
        let id = $('select[name="nopinjam"]').val();
        event.preventDefault();   
    
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/r/submit_pengembalian');?>",
                data: { id_detail:id}
                })
                .done(function( msg ) {
                
                    var res=JSON.parse(msg);                             
                    if(res.status=='1'){
                        
                        table.ajax.reload();
                        console.log('ok');
                        $('.text-ok').html(res.msg);
                        $(".alert-ok").fadeTo(2000, 1000).slideUp(1000, function() {
                                $(".alert-ok").slideUp(1000);
                                $('.barcodesearch').val('');
                                $('.barcodesearch').prop( "disabled", false );
                                $('.barcodesearch').focus();
                        });
                    }                           
                    else
                    {
                        console.log('gagal');
                        $('.text-gagal').html(res.msg);
                        $(".alert-gagal").fadeTo(2000, 1000).slideUp(1000, function() {
                                $(".alert-gagal").slideUp(1000);
                                $('.barcodesearch').val('');
                                $('.barcodesearch').prop( "disabled", false );
                                $('.barcodesearch').focus();
                        });

                    }

                    
                    return false;
                });
    });
    // form submit
    $( "#form-kembali" ).submit(function( event ) {
        $('.barcodesearch').prop( "disabled", true );
                var vv=$('.barcodesearch').val();
                console.log(vv);
                console.log('ditemukan');
                
                var search=$('.searchby :selected').val();
                $.ajax({
                                method: "POST",
                                url: "<?php echo base_url('panel/r/proses_pengembalian');?>",
                                data: { nomor: vv ,searchby:search}
                                })
                                .done(function( msg ) {
                                
                                    var res=JSON.parse(msg);                             
                                    if(res.status=='1'){
                                        
                                        table.ajax.reload();
                                        console.log('ok');
                                        $('.text-ok').html(res.msg);
                                        $(".alert-ok").fadeTo(2000, 1000).slideUp(1000, function() {
                                                $(".alert-ok").slideUp(1000);
                                                $('.barcodesearch').val('');
                                                $('.barcodesearch').prop( "disabled", false );
                                                $('.barcodesearch').focus();
                                        });
                                    }                                
                                    else if(res.status=='2'){
                                        console.log('Pinjaman Lebih dari 1');
                                        $('#selectpinjam').show();
                                        $('#tbok').show();  
                                        $('.modal-title').html('Pilih Pinjaman');  
                                        $('.text-desc').html(res.pinjaman);
                                        $('.barcodesearch').prop( "disabled", false );
                                        $('#modal-result').modal('show');
                                        let v = res.pinjaman;
                                    //  console.log(v);
                                        $('select[name="nopinjam"]').empty();
                                        for(var a=0;a<v.length;a++){                         
                                                $('select[name="nopinjam"]').append('<option value="'+v[a]['id_detail']+'">'+v[a]['pid']+' | '+v[a]['nama']+' | '+v[a]['tanggal']+'</option>');
                                        }
                                        $('select[name="nopinjam"]').selectpicker('refresh');
                                    }else
                                    {
                                        console.log('gagal');
                                        $('.text-gagal').html(res.msg);
                                        $(".alert-gagal").fadeTo(2000, 1000).slideUp(1000, function() {
                                                $(".alert-gagal").slideUp(1000);
                                                $('.barcodesearch').val('');
                                                $('.barcodesearch').prop( "disabled", false );
                                                $('.barcodesearch').focus();
                                        });

                                    }

                                    
                                    return false;
                                });

                
                return false;    //<---- Add this line
    });
    // DETECT BARCODE
    $('.barcodesearch').keypress(function (e) {
        if (e.which == 13) {
            $(this).prop( "disabled", true );
            var vv=$(this).val();
            console.log(vv);
            console.log('ditemukan');
            
            var search=$('.searchby :selected').val();
            $.ajax({
                            method: "POST",
                            url: "<?php echo base_url('panel/r/proses_pengembalian');?>",
                            data: { nomor: vv ,searchby:search}
                            })
                            .done(function( msg ) {
                            
                                var res=JSON.parse(msg);                             
                                if(res.status=='1'){
                                    
                                    table.ajax.reload();
                                    console.log('ok');
                                    $('.text-ok').html(res.msg);
                                    $(".alert-ok").fadeTo(2000, 1000).slideUp(1000, function() {
                                            $(".alert-ok").slideUp(1000);
                                            $('.barcodesearch').val('');
                                            $('.barcodesearch').prop( "disabled", false );
                                            $('.barcodesearch').focus();
                                    });
                                }                                
                                else if(res.status=='2'){
                                    console.log('Pinjaman Lebih dari 1');
                                    $('#selectpinjam').show();
                                    $('#tbok').show();
                                    $('.modal-title').html('Pilih Pinjaman');  
                                    $('.text-desc').html(res.pinjaman);
                                    $('.barcodesearch').prop( "disabled", false );
                                    $('#modal-result').modal('show');
                                    let v = res.pinjaman;
                                  //  console.log(v);
                                    $('select[name="nopinjam"]').empty();
                                    for(var a=0;a<v.length;a++){                         
                                              $('select[name="nopinjam"]').append('<option value="'+v[a]['id_detail']+'">'+v[a]['pid']+' | '+v[a]['nama']+' | '+v[a]['tanggal']+'</option>');
                                    }
                                    $('select[name="nopinjam"]').selectpicker('refresh');
                                }else
                                {
                                    console.log('gagal');
                                    $('.text-gagal').html(res.msg);
                                    $(".alert-gagal").fadeTo(2000, 1000).slideUp(1000, function() {
                                            $(".alert-gagal").slideUp(1000);
                                            $('.barcodesearch').val('');
                                            $('.barcodesearch').prop( "disabled", false );
                                            $('.barcodesearch').focus();
                                    });

                                }

                                
                                return false;
                            });

            
            return false;    //<---- Add this line
        }
    });

    $(document).ready(function(){

        
        // selectpicker
        $('select').selectpicker();


        // Setup datatables
        table = $('#tabel-master-katalog').DataTable({ 
            "language": 
                {
                "url" :"<?php echo base_url('assets/plugins/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [['1','desc']], //init datatable not ordering
            ajax: "<?php echo base_url('panel/r/pengembalian_ajax')?>",
            
            columnDefs: [
                
                { targets: 0, className: 'dt-body-nowrap text-center',orderable: false}, //last column center.
                { targets: 1, className: 'dt-body-nowrap text-center'}, //last column center. 
                { targets: 2, className: 'dt-body-nowrap text-center'}, //last column center. 
                { targets: 3, className: 'dt-body-nowrap text-center'}, //last column center. 
            
            
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
            
            {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
            },
            {  extend: 'excel',
                title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
            },
            {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                    messageTop :'<?php echo $title.' '.date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
            },
        ],
        });


        
 
           
                
                    
            
    });
</script>

<?= $this->endSection() ?>