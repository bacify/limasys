
<?php $this->extend('admin/page_layout'); ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Katalog Kategori</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Katalog Kategori</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Database katalog</h3>
                <button class="btn btn-success float-right" data-toggle="modal" data-target="#myModalAdd">Tambah Baru</button>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover table-sm" width="100%">
                <thead>
                        <tr>
                        <th>No</th>
                        <th>Nama Kategori</th>                        
                        <th >Tindakan</th>
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->



         <!-- Modal Add Product-->
         <form id="form-tambah-buku" action="<?php echo base_url('panel/kategori_save');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Tambah katalog Baru</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                        <label for="no_register" class="mb-0 pb-0">Nama Kategori</label>
                             
                           <input type="text" name="nama_kategori" class="form-control" placeholder="Kategori">
                       </div>
                        
                                         
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
 <!-- Modal View Product-->
<form id="updateform" class="form-horizontal" action="<?php echo site_url('panel/kategori_save');?>" method="post">

         <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content ">
                   <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Kategori Katalog</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <div class="form-group row">
                        <label for="no_register" class="col-md-2  control-label my-auto py-auto">Nama kategori</label>
                            <div class="col-md-10">
                            <input type="hidden" name="id" value="0" class="form-control">
                           <input type="text" name="nama_kategori" class="form-control" placeholder="Kategori" required>
                           </div>
                       </div>                        
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Update</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
 
     <!-- Modal delete Product-->
      <form id="deleteform" action="<?php echo base_url('panel/kategori_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Katalog</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  

<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js');?>"></script>
 

<script>
    $(document).ready(function(){
        // selectpicker
        $('select').selectpicker();

        
        $(document).ready(function() {
        table = $('#tabel-master-katalog').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/plugins/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo site_url('panel/kategoriajax')?>",
            
            columnDefs: [
                
                { targets: -1, className: 'text-nowrap' ,width: '5%'}, //last column center.
               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1,  ]
                }
             },
             {  extend: 'excel',
                title: '<?php echo $title.' '.date('d-m-Y'); ?>  ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1,   ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: '<?php echo $title.' '.date('d-m-Y'); ?>  ',
                    messageTop :'<?php echo $title.' '.date('d-m-Y'); ?>  ',
                    columns: [ 0, 1,  ]
                }
             },
         ],
        });
    });
    

             

            // get Edit Records
            $('#tabel-master-katalog').on('click','.edit_record',function(){

                let id=$(this).data('id');
                let nama=$(this).data('nama');
                $('#ModalUpdate').modal('show');
                    $('#updateform [name="id"]').val(id);
                    $('#updateform [name="nama_kategori"]').val(nama);
                
                  
                        
      });
            // End Edit Records
            // get delete Records
            $('#tabel-master-katalog').on('click','.delete_record',function(){
            var id=$(this).data('id');
            $('#ModalDelete').modal('show');
            $('#deleteform [name="id"]').val(id);
      });
            // End delete Records


            
    });
</script>
<?= $this->endSection() ?>