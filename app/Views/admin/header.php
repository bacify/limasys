<?php 
            
            $uri= current_url(true);
            $link1=$uri->getSegment(1);
            $link2=$uri->getSegment(2);

            ?>

<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark navbaratas">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button">
          <i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Library Management System</a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url(); ?>assets/index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>

    <!-- SEARCH FORM -->
    <!-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> -->
    <ul class="navbar-nav ml-auto">
      <?php if(session()->get('UID')){
          
          ?>		
          <li class="nav-item">
          <a class="nav-link" href="#">          
          <span><i class="fas fa-user-lock" title="Admin"></i>  <?=session()->get('nama');?>
        </a>
          
        
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php if(session()->get('type')=='admin') echo base_url('panel/logout'); else echo base_url('member/logout');?>">          
          <i class="fas fa-sign-out-alt" title="Keluar"></i>
        </a>
      </li>

      <?php } ?>
    </ul>
    
  </nav>
  <!-- /.navbar -->

   <!-- Main Sidebar Container -->
  <aside class="main-sidebar  elevation-4 sidebar-utama">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?php echo base_url('assets/img/limasys_logo.png'); ?>"
           alt="LIMASYS Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><?php if( session()->get('type')=='admin') echo 'Admin Panel'; else echo 'Member Area';?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->

        <!-- ADMIN MENU -->
        <?php if(session()->get('type')=='admin'){ ?>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" <?php if(!session()->get('UID')){ echo 'style="display:none"';}?>>
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo base_url('panel');?>" class="nav-link <?php if( $link1=='panel' && $link2=='home' ) echo 'active';?>">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Halaman Utama
                
              </p>
            </a>
            
          </li>
          <?php 
          $statustree=false;
          if($link2=='inventory' || $link2=='kategori'|| $link2=='katalog' || $link2=='stok_katalog' || $link2=='riwayat_inventory' || $link2 =='i')
          {
            $statustree=true;
          }
          /* $statustree2=false;
          if($link2=='kategori_katalog' || $link2=='useradmin')
          {
            $statustree2=true;
          } */

          ?>
          <li class="nav-item has-treeview <?php if($link1=='panel' && $statustree ) echo "menu-open";?>">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-book"></i>
              <p>
                  Inventory
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="<?php echo base_url('panel/kategori');?>" class="nav-link <?php if( $link1=='panel' && $link2=='kategori' ) echo 'active';?>">
                <i class="far fa-circle nav-icon"></i>
                    <p>
                      Kategori katalog 
                    </p>
                  </a>
                  </a>
              </li>
            <li class="nav-item">
              <a href="<?php echo base_url('panel/katalog');?>" class="nav-link <?php if( $link1=='panel' && $link2=='katalog' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Db Katalog
                  </p>
                </a>
                </a>
              </li>
              
              <li class="nav-item">
              <a href="<?php echo base_url('panel/stok_katalog');?>" class="nav-link <?php if($link1=='panel' && $link2=='stok_katalog'  ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Stok                
                  </p>
                </a>
                </a>
              </li>
              <li class="nav-item">
              <a href="<?php echo base_url('panel/inventory');?>" class="nav-link <?php if($link1=='panel' &&  ($link2=='inventory' ||$link2 == 'i') ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                  inventaris           
                  </p>
                </a>
                </a>
              </li> 
              <li class="nav-item">
              <a href="<?php echo base_url('panel/riwayat_inventory');?>" class="nav-link <?php if($link1=='panel' && $link2=='riwayat_inventory' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Riwayat          
                  </p>
                </a>
                </a>
              </li>             
            </ul>
            
            
          </li>

          <li class="nav-item">

            <a href="<?php echo base_url('panel/kunjungan');?>" class="nav-link <?php if( $link1=='panel' && $link2=='kunjungan' ) echo 'active';?>">

              <i class="nav-icon fas fa-eye"></i>
              <p>
                Kunjungan
                 
              </p>
            </a>
            </li>
            <?php 
            $statustree4=false;
            
              if($link2=='pinjaman' || $link2=='pengembalian' || $link2=='daftar_pinjaman'||$link2=='p')
              {
                $statustree4=true;
              }

            ?>
            <li class="nav-item has-treeview <?php if($link1=='panel' && $statustree4 ) echo "menu-open";?>">

            <a href="#" class="nav-link">

              <i class="nav-icon fas fa-clipboard"></i>
              <p>
                Pinjaman Buku
                <i class="fas fa-angle-left right"></i>      
              </p>
            </a>
            <ul class="nav nav-treeview">
           
            <li class="nav-item">
              <a href="<?php echo base_url('panel/pinjaman');?>" class="nav-link <?php if( $link1=='panel' && ($link2=='pinjaman' ||$link2=='p') ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Daftar pinjaman
                  </p>
                </a>
                </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('panel/pengembalian');?>" class="nav-link <?php if( $link1=='panel' &&  $link2=='pengembalian') echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    pengembalian
                  </p>
                </a>
                </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('panel/daftar_pinjaman');?>" class="nav-link <?php if( $link1=='panel' && $link2=='daftar_pinjaman' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Daftar buku dipinjam
                  </p>
                </a>
                </a>
            </li>
            
            </ul>
            </li>
            
            
            <?php 
            $statustree3=false;
               
              if($link2=='admin_list' || $link2=='member_list')
              {
                $statustree3=true;
              }

            ?>
            <li class="nav-item has-treeview <?php if($link1=='panel' && $statustree3 ) echo "menu-open";?>">

            <a href="#" class="nav-link">

              <i class="nav-icon fas fa-users"></i>
              <p>
                Daftar Pengguna     
                <i class="fas fa-angle-left right"></i>         
              </p>
            </a>
            <ul class="nav nav-treeview">
           
            <li class="nav-item">
              <a href="<?php echo base_url('panel/admin_list');?>" class="nav-link <?php if( $link1=='panel' && $link2=='admin_list' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Admin
                  </p>
                </a>
                </a>
            </li>

            <li class="nav-item">
              <a href="<?php echo base_url('panel/member_list');?>" class="nav-link <?php if( $link1=='panel' && $link2=='member_list' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Member
                  </p>
                </a>
                </a>
            </li>
            </ul>
              <?php

              $statustree5=false;
              if($link2=='kategori_katalog' || $link2=='konfigurasi'|| $link2=='ganti_password')
              {
                $statustree2=true;
              }
              ?>

            </li>
            <!-- <li class="nav-item has-treeview <?php if($link1=='panel' && $statustree5 ) echo "menu-open";?>">
              <a href="#" class="nav-link ">
                <i class="nav-icon fas fa-cog"></i>
                <p>
                  Pengaturan
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>

              <ul class="nav nav-treeview">
            
              <li class="nav-item">
                <a href="<?php echo base_url();?>panel/kategori_katalog" class="nav-link <?php if( $link1=='panel' && $link2=='kategori_katalog' ) echo 'active';?>">
                <i class="far fa-circle nav-icon"></i>
                    <p>
                      Kategori katalog 
                    </p>
                  </a>
                  </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?>panel/konfigurasi" class="nav-link <?php if( $link1=='panel' && $link2=='konfigurasi' ) echo 'active';?>">
                <i class="far fa-circle nav-icon"></i>
                    <p>
                      Pengaturan Aplikasi
                    </p>
                  </a>
                  </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?>panel/ganti_password" class="nav-link <?php if( $link1=='panel' && $link2=='ganti_password' ) echo 'active';?>">
                <i class="far fa-circle nav-icon"></i>
                    <p>
                      Ganti password
                    </p>
                  </a>
                  </a>
              </li>

            
              </ul>
            </li>           -->
           

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
        
          <!-- END OF ADMIN MENU -->
          <?php } else{?>
          <!-- MEMBER MENU -->
          <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" <?php if(!session()->get('UID')){ echo 'style="display:none"';}?>>
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo base_url();?>member/home" class="nav-link <?php if( $link1=='member' && $link2=='home' ) echo 'active';?>">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Halaman Utama
                
              </p>
            </a>
            
          </li>
          <?php 
          $statustree=false;
          if($link2=='password')
          {
            $statustree=true;
          }
          // $statustree2=false;
          // if($link2=='kategori_katalog' || $link2=='useradmin')
          // {
          //   $statustree2=true;
          // }

          ?>
          <li class="nav-item">
            <a href="<?php echo base_url();?>member/member_daftar_pinjaman" class="nav-link ">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Daftar Pinjaman
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url();?>member/member_riwayat" class="nav-link ">
               <i class="nav-icon fas fa-clipboard"></i>
              <p>
               Riwayat Pinjaman
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url();?>member/member_booking" class="nav-link ">
              <i class="nav-icon fas fa-chalkboard-teacher"></i>
              <p>
                Online Booking
                
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview <?php if($link1=='member' && $statustree ) echo "menu-open";?>">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-book"></i>
              <p>
                  Profil
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
           
            <li class="nav-item">
              <a href="<?php echo base_url();?>member/password" class="nav-link <?php if( $link1=='member' && $link2=='password' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Ubah password 
                  </p>
                </a>
                </a>
            </li>
          </ul>
        </nav>
          <?php }?>
          <!-- END OF MEMBER MENU -->

    </div>
    <!-- /.sidebar -->
  </aside> 