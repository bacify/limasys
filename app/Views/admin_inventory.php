
<?php $this->extend('admin/page_layout'); ?>

<?= $this->section('content') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Transaksi Katalog Masuk</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Transaksi Katalog Masuk</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if( session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo  session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Transaksi</h3>
                <button class="btn btn-success float-right" data-toggle="modal" data-target="#myModalAdd">Tambah Baru</button>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover table-sm" width="100%">
                <thead>
                        <tr>
                        <th>NoTransaksi</th> 
                        <th>Tanggal</th> 
                        <th>Jenis</th> 
                        <th>Asal</th>
                        <th>Keterangan</th>
                        <th>Operator</th> 
                        <th>Status</th> 
                        <th>Tindakan</th> 
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


 <!-- Modal Add Product-->
 <form id="form-tambah-buku" action="<?php echo base_url('panel/new_inventory');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Transaksi Katalog</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="kategori" class="mb-0 pb-0">Jenis Transaksi</label>
                            <select name="jenis" class="form-control" data-live-search="true" title="Jenis Katalog" required> 
                                <option value="1">Masuk</option>
                                <option value="2">Keluar</option>
                                                 </select>
                       </div>
                        <div class="form-group">
                        <label for="Asal" class="mb-0 pb-0">Asal</label>
                             
                           
                           <input type="hidden" name="status_transaksi" value="0">
                           <input type="text" name="asal" class="form-control" placeholder="Asal Katalog">
                       </div>
                        
                       <div class="form-group">
                            <label for="keterangan" class="mb-0 pb-0">Keterangan</label>
                           <input type="text" name="keterangan" class="form-control" placeholder="keterangan" >
                       </div>                           
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 

 <!-- Modal Update Product-->
<form id="updateform" action="<?php echo base_url('panel/new_inventory');?>" method="post">
         <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content">
                   <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Update Transaksi <strong class="idmaster"></strong></h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                   <div class="form-group">
                            <label for="kategori" class="mb-0 pb-0">Jenis Transaksi</label>
                            <select name="jenis" class="form-control" data-live-search="true" title="Jenis Katalog" required> 
                                <option value="1">Masuk</option>
                                <option value="2">Keluar</option>
                                                 </select>
                       </div>
                        <div class="form-group">
                        <label for="Asal" class="mb-0 pb-0">Asal</label>
                          <input type="hidden" name="id_master" value="0">                           
                           <input type="text" name="asal" class="form-control" placeholder="Asal Katalog">
                       </div>
                        
                       <div class="form-group">
                            <label for="keterangan" class="mb-0 pb-0">Keterangan</label>
                           <input type="text" name="keterangan" class="form-control" placeholder="keterangan" >
                       </div>                           
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Update</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  
  <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo base_url('panel/delete_inventory');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Transaksi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>


<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js');?>"></script>
 
<script>
    
    $(document).ready(function(){
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        
        table = $('#tabel-master-katalog').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/plugins/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo site_url('panel/inventoryajax')?>",
            
            columnDefs: [
                
                { targets: 2, className: 'text-nowrap text-center'}, //last column center.
                { targets: 5, className: 'text-center'}, //last column center.
               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'excel',
                title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                    messageTop :'<?php echo $title.' '.date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
         ],
        });
     
    

       
 
            // end setup datatables


            $('#tabel-master-katalog').on('click','.view_record',function(){

                    var id=$(this).data('id');
                    $.ajax({
                    dataType: "json",
                    url: '<?php echo base_url();?>dashboard/get_katalog_json/'+id,                
                    success:function(data) { 
                        
                        $('#ModalView').modal('show');                       
                        
                    }
                    });

                            
                          
                            

                            
});


            // get Edit Records
            $('#tabel-master-katalog').on('click','.edit_record',function(){

                var id=$(this).data('id');
                $.ajax({
                dataType: "json",
                url: '<?php echo base_url('panel/inventoryjson');?>/'+id,                
                success:function(data) { 
                    
                    $('#ModalUpdate').modal('show');
                    $('#updateform [name="id_master"]').val(data.id_master);
                    $('#updateform [name="jenis"]').selectpicker('val',data.jenis_transaksi);                    
                    $('#updateform [name="jenis"]').selectpicker('destroy');                    
                    $('#updateform [name="jenis"] option:not(:selected)').prop('disabled', true);
                    $('#updateform [name="jenis"] option:selected ').prop('disabled', false);
                    $('#updateform [name="asal"]').val(data.asal);
                    $('#updateform [name="keterangan"]').val(data.keterangan);
                    $('#updateform .idmaster').html(pad(id,6));
                    
                }
                });
                 
                    
                        
           
                        
      });
            // End Edit Records
            // get delete Records
            $('#tabel-master-katalog').on('click','.delete_record',function(){
            var id=$(this).data('id');
            $('#ModalDelete').modal('show');
            $('#deleteform [name="id"]').val(id);
      });
            // End delete Records


            
    });


  function pad (str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
  }
</script>


<?= $this->endSection() ?>