
<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>

<div class="row pt-5 py-auto mb-5 h-auto   justify-content-center align-items-center">
      <div class="col-6 col-md-6 col-lg-4 card bg-login card-custom">
      <div class="notification mt-1 mb-0 pt-1"><center><p class="bg-light text-danger my-0 py-0"><strong><?php echo session()->getFlashdata('error');?></strong></p></center></div>
        <div class="card-header bg-transparent py-0">
          <center><h2 class="text-light  my-0" >Akun</h2></center>
          </div>
        <div class="card-body">
            <form class="form form-cari " action="<?=base_url('login/auth');?>" method="POST">
            
            <div class="form-group">
                <label class="text-light" for="formGroupExampleInput">Username</label>
                <input type="email" name="username" class="form-control form-control-lg  " placeholder="masukkan Email" aria-label="Recipient's Username" aria-describedby="basic-addon2" required>
            </div>
            <div class="form-group">
                <label class="text-light" for="formGroupExampleInput">Password</label>
                <input type="password" name="password" class="form-control form-control-lg  " placeholder="Password" aria-label="Recipient's Password" aria-describedby="basic-addon2" required>
            </div>
            <div class="form-group row pt-3">
                <div class="col-8 offset-2 ">
                <button type="submit" name="submit" value="submit" class="btn btn-dark btn-lg btn-block" style="background-color:#312450">Masuk</button>

                </div>
            </div>
                
                
            </form>  
        </div>
        <div class="card-footer bg-transparent py-1">
        <small id="emailHelp" class="form-text text-light">Silahkan klik <a href="<?php echo base_url();?>main/lupapassword" class="badge badge-dark"> disini</a> jika lupa password.</small>
        </div>
     </div>
    </div>
 
    <?= $this->endSection() ?>
 
 