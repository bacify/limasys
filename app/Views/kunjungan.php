<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
<div class="row h-50 justify-content-center align-items-center">
      <div class="col-8 col-md-8 col-lg-6 content-cari">


      <div class="card bg-result">
        <div class="card-header"><h5 class="text-dark card-title"><?=$title?></h5></div>
      <div class="card-body">
      
          <form class="form  bg-info p-1 mx-1" action="#" method="POST">
            
              <input type="text" name="nomember" id="noidmember" class="form-control   nomember  " placeholder="Input No Member / identitas" aria-label="Recipient's username"  onmouseover="this.focus();" maxlength="6">
            
          </form>  
      </div>
      <div class="card-footer px-4" >
        <table class="table table-sm table-dark table-striped mx-auto hasil " style="display:none" >
        <thead>
            <tr>
            <th colspan="2" class="text-center text-title">Data Member</th> 
            </tr>
        </thead>
            <tbody>
            <tr>
                <td>No Member</td>
                <td class="nowrap">: <span id="nomember" class="font-weight-normal">000001</span></td>
            </tr>
            <tr>
                <td>Nama</td>
                <td class="nowrap">: <span id="nama" class="font-weight-normal">Bachtiar Arif</span></td>
            </tr>
            <tr>
                <td>jenis</td>
                <td class="nowrap">: <span id="jenis" class="font-weight-normal">Member</span></td>
            </tr>
            </tbody>
        </table>
            

      </div>
    </div>
        
     </div>
    </div>
 
    <!-- MODAL HASIL -->
    <div class="modal fade " id="modal-result" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm ">
               <div class="modal-content ">
                   <div class="modal-header">
                   <h5 class="modal-title text-center" id="exampleModalLongTitle">Hasil</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body text-center">                        
                        <h2 class="text-result"><strong class="text-uppercase text-primary">Data DITEMUKAN</strong></h2>
                        <p class="text-muted text-desc">deskripsi</p> 
                   </div>
                   <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">OK</button>
                     
                  </div>
                    
                </div>    
          </div>
    </div>
 
<?= $this->endSection() ?>
<?= $this->section('jslibrary') ?>
<script>
  $("#noidmember" ).focus();

  
// SET IDENTITAS NUMBER ONLY
setInputFilter(document.getElementById("noidmember"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
        }
 $( "#noidmember" ).keypress(function(e) {
            var value=$(this).val();
            console.log(value);
            if(e.which == 13){
                  $('.text-result').html('');
                  $('.text-desc').html('');
                        $.ajax({
                                method: "GET",
                                dataType: "json",
                                url: "<?php echo base_url('sys/insert');?>/"+value,                        
                                })
                                .done(function( msg ) {
                                    $("#nama").html('');
                                    $("#nomember").html('');
                                    $("#jenis").html('');
                                if(msg.status=='1'){
                                    $(".text-title").html('Data Member Ditemukan!');
                                    $("#nama").html(msg.result.nama);
                                    $("#nomember").html(msg.result.id_member);
                                    if(msg.result.type == 1)
                                        $("#jenis").html('Member');
                                    else if(msg.result.type == 2)
                                        $("#jenis").html('Siswa');
                                    else
                                        $("#jenis").html('Member');

                                    $(".hasil").fadeTo(2000, 1000).slideUp(100000, function() {
                                            $(".hasil").slideUp(100000);
                                            $("#noidmember" ).val('');
                                            $("#noidmember" ).focus();
                                    });
                                    console.log(msg)
                                  $('#modal-result').modal('toggle');
                                  $('.text-result').html('<strong class="text-uppercase text-primary">OK!</strong>');
                                  setTimeout(function(){
                                      $('#modal-result').modal('hide');
                                      $("#noidmember" ).val('');
                                      $("#noidmember" ).focus();
                                    }, 2000);
                                } else{
                                  $(".text-title").html('Data Member Tidak');
                                  $('#modal-result').modal('toggle');
                                  $('.text-result').html('<strong class="text-uppercase text-danger">GAGAL! Member Tidak Ditemukan</strong>');
                                  $('.text-desc').html('<strong class="text-danger">*silahkan hubungi admin!</strong>');
                                  setTimeout(function(){
                                      $('#modal-result').modal('hide');
                                      $("#noidmember" ).val('');
                                      $("#noidmember" ).focus();
                                    }, 3000);
                                }


                                    return false;
                                });
                return false;    //<---- Add this line
                }
            });
</script>
<?= $this->endSection() ?>