
<?php $this->extend('admin/page_layout'); ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?php if(isset($title)) echo $title;?></h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?php if(isset($title)) echo $title;?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?php if(isset($title)) echo $title;?></h3>
                 
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover table-sm" width="100%">
                <thead>
                        <tr>
                        <th>No</th> 
                        <th>judul</th>
                        <th>pengarang</th>
                        <th>penerbit</th>                        
                        <th>kategori</th>
                        <th>Stok</th>
                        <th>Cetak</th>
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->



<!-- QR Code Form-->
<form id="barcode" action="<?php echo site_url('panel/barcode');?>" method="GET">
         <div class="modal fade" id="ModalBarcode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Cetak <span class="texttitel">BARCODE</span></h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="type" class="form-control" value="1">
                           <input type="hidden" name="id" class="form-control" >
                           <div class="form-group">
                        <label for="kategori" class="mb-0 pb-0">Field yang akan dicetak </label>
                           <select name="field" class="form-control" data-live-search="true" title="silahkan pilih" required>                                         
                                                <option value="id">Nomor katalog</option>
                                                <option value="register">Nomor Register</option>
                                                <option value="isbn">ISBN</option>
                                                      
                                                 </select>
                       </div>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit"  class="btn btn-success">Submit</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  

<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js');?>"></script>
 

<script>
    $(document).ready(function(){
        // selectpicker
        $('select').selectpicker();

        
        $(document).ready(function() {
        table = $('#tabel-master-katalog').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/plugins/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo site_url('panel/katalogstokajax')?>",
            
            columnDefs: [
                
                { targets: -1, className: 'text-nowrap'}, //last column center.
                { targets: -1, className: 'text-center'}, //last column center.
               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'excel',
                title: 'Data Layanan Aktif <?php echo date('d-m-Y'); ?> ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: 'Data Layanan Aktif <?php echo date('d-m-Y'); ?> ',
                    messageTop :'Data Layanan Aktif <?php echo date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
         ],
        });
    });
    
            // end setup datatables


            $('#tabel-master-katalog').on('click','.view_record',function(){

                    var id=$(this).data('id');
                    $.ajax({
                    dataType: "json",
                    url: '<?php echo base_url();?>dashboard/get_katalog_json/'+id,                
                    success:function(data) { 
                        console.log(data);
                        $('#ModalView').modal('show');
                        $('#viewform [name="id"]').val(data.id_katalog);
                        $('#viewform [name="no_register"]').val(data.no_register);
                        $('#viewform [name="judul"]').val(data.judul);
                        $('#viewform [name="no_panggil"]').val(data.no_panggil);
                        $('#viewform [name="pengarang"]').val(data.pengarang);
                        $('#viewform [name="penerbit"]').val(data.penerbit);
                        $('#viewform [name="kategori"]').selectpicker('val', data.kategori_id);
                        $('#viewform [name="bahasa"]').val(data.bahasa);
                        $('#viewform [name="tahun"]').val(data.tahun);
                        $('#viewform [name="edisi"]').val(data.edisi);
                        $('#viewform [name="subyek"]').val(data.subyek);
                        $('#viewform [name="klasifikasi"]').val(data.klasifikasi);
                        $('#viewform [name="deskripsi"]').val(data.deskripsi);
                        $('#viewform [name="isbn"]').val(data.isbn);
                        
                    }
                    });

                            
                          
                            

                            
});


            // get Edit Records
            $('#tabel-master-katalog').on('click','.edit_record',function(){

                var id=$(this).data('id');
                $.ajax({
                dataType: "json",
                url: '<?php echo base_url();?>dashboard/get_katalog_json/'+id,                
                success:function(data) { 
                    console.log(data);
                    $('#ModalUpdate').modal('show');
                    $('#updateform [name="id"]').val(data.id_katalog);
                    $('#updateform [name="no_register"]').val(data.no_register);
                    $('#updateform [name="judul"]').val(data.judul);
                    $('#updateform [name="no_panggil"]').val(data.no_panggil);
                    $('#updateform [name="pengarang"]').val(data.pengarang);
                    $('#updateform [name="penerbit"]').val(data.penerbit);
                    $('#updateform [name="kategori"]').selectpicker('val', data.kategori_id);
                    $('#updateform [name="bahasa"]').val(data.bahasa);
                    $('#updateform [name="tahun"]').val(data.tahun);
                    $('#updateform [name="edisi"]').val(data.edisi);
                    $('#updateform [name="subyek"]').val(data.subyek);
                    $('#updateform [name="klasifikasi"]').val(data.klasifikasi);
                    $('#updateform [name="deskripsi"]').val(data.deskripsi);
                    $('#updateform [name="isbn"]').val(data.isbn);
                    
                }
                });
                 
                    
                        
           
                        
      });
            // End Edit Records
            // get delete Records
       $('#tabel-master-katalog').on('click','.qrcodebtn',function(){
            var id=$(this).data('id');
            $('#ModalBarcode').modal('show');
            $('#barcode [name="type"]').val(1);
            $('#barcode [name="id"]').val(id);
            $('.texttitel').html('QrCode');
       });
       $('#tabel-master-katalog').on('click','.barcodebtn',function(){
            var id=$(this).data('id');
            $('#ModalBarcode').modal('show');
            $('#barcode [name="type"]').val(2);

            $('.texttitel').html('BarCode');
            $('#barcode [name="id"]').val(id);
       });
            // End delete Records


            
    });
</script>
<?= $this->endSection() ?>