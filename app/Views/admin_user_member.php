
<?php $this->extend('admin/page_layout'); ?>

<?= $this->section('content') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?=$title;?></h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?=$title;?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Member</h3>
                <button class="btn btn-success float-right" onclick="adduser()">Tambah Baru</button>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover table-sm" width="100%">
                <thead>
                        <tr>
                        <th>No</th> 
                        <th>NoMember</th> 
                        <th>Nama</th>
                        <th>Identitas</th>
                        <th>Alamat</th>
                        <th>Kec</th>
                        <th>Kota</th>
                        <th>Provinsi</th>
                        <th>kodepos</th>
                        <th>Telp</th>                        
                        <th>status</th>                         
                        <th>tindakan</th>
                         
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


 

   <!-- Modal Add Product-->
   <form id="form-tambah-user" action="<?php echo base_url('panel/m/member_add');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Buat Member baru</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                        <label for="no_register" class="mb-0 pb-0">Nama</label>                             
                           <input type="text" name="nama" class="form-control" placeholder="nama" required>
                       </div>
                       <div class="form-group">
                            <label for="identitas" class="mb-0 pb-0">nomor identitas</label>
                           <small class="statusid"> 
                                <div class="spinner-border spinner-border-sm  loadingiconid" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="fas fa-times-circle ikonid-gagal" style="display:none"> Identitas sudah terpakai</i>
                                <i class="fas fa-check-circle ikonid-ok"style="display:none"></i>
                                 </small>
                            <input type="text" id="identitas" name="identitas" class="form-control " placeholder="" required>
                                                    
                       </div>
                       <div class="form-group">
                            <label for="alamat" class="mb-0 pb-0">alamat</label>
                           <input type="text" name="alamat" class="form-control" placeholder="Jl Diponegoro 01" >
                       </div>
                       <div class="form-group">
                            <label for="provinsi" class="mb-0 pb-0">Provinsi</label>
                            <select name="provinsi" class="form-control provinsi" data-live-search="true" title="Silahkan Pilih" required>
                                    <?php 
                                    foreach($provinsi as $de){
                                        echo '<option value="'.$de['prov_id'].'">'.$de['prov_name'].'</option>';
                                    }                                                                                            
                                    ?>                                    
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="kota" class="mb-0 pb-0">Kota</label>
                            <select name="kota" class="form-control kota" data-live-search="true" title="Silahkan Pilih" required>
                                <option disabled>silahkan isi provinsi</option>                                                                  
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="kec" class="mb-0 pb-0">Kec</label>
                            <select name="kec" class="form-control kec" data-live-search="true" title="Silahkan Pilih" required>
                                <option disabled>silahkan isi Kota</option>                                                                  
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="desa" class="mb-0 pb-0">Desa</label>
                            <select name="desa" class="form-control desa" data-live-search="true" title="Silahkan Pilih" required>
                                <option disabled>silahkan isi Kota</option>                                                                  
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="kodepos" class="mb-0 pb-0">Kodepos</label>
                           <input type="text" name="kodepos" class="form-control" placeholder="65141" >
                       </div>
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">telp</label>
                           <input type="tel" name="telp" class="form-control" placeholder="0812333111" >
                       </div>
                       <div class="form-group">
                            <label for="email" class="mb-0 pb-0">Email</label>
                            <small class="statusemail"> 
                                <div class="spinner-border spinner-border-sm  loadingiconemail" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="fas fa-times-circle ikonemail-gagal" style="display:none"> Email sudah terpakai</i>
                                <i class="fas fa-check-circle ikonemail-ok"style="display:none"></i>
                                 </small>
                           <input type="email" name="email" class="form-control" placeholder="contoh@gmail.com" >
                       </div>
                        <div class="form-group">
                            <label for="password" class="mb-0 pb-0">Password</label>
                           <input type="text" name="password" class="form-control" placeholder="password" required>
                       </div>

                       <div class="form-group">
                        <label for="kategori" class="mb-0 pb-0">jenis</label>
                           <select name="tipe" class="form-control" data-live-search="true" title="silahkan pilih" required>                                                                                               
                                                <option value="1">Member</option>                                                        
                                                <option value="2">Siswa</option>                                                        
                                                <option value="3">Guru</option>                                                        
                                                 </select>
                       </div>
                        <div class="form-group row">
                                <label for="status" class="mb-0 py-auto col-lg-1 col-sm-3">Status</label>
                                <div class="col">
                                    <input type="checkbox" name="status" value="1"  data-toggle="toggle" data-on="Aktif" data-off="Tidak Aktif" data-size="normal" data-width="200" data-onstyle="success" data-offstyle="danger">
                                </div>
                       </div>                                  
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>




   <!-- Modal Edit Member-->
   <form id="form-update-user" action="<?php echo base_url('panel/m/member_update');?>" method="post">
         <div class="modal fade " id="myModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Edit Data Member</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                        <label for="no_register" class="mb-0 pb-0">Nama</label>                             
                           <input type="hidden" name="id" value="0">
                           <input type="text" name="nama" class="form-control" placeholder="nama" required>
                       </div>
                       <div class="form-group">
                            <label for="identitas" class="mb-0 pb-0">nomor identitas</label>
                           <small class="statusid"> 
                                <div class="spinner-border spinner-border-sm  loadingiconid" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="fas fa-times-circle ikonid-gagal" style="display:none"> Identitas sudah terpakai</i>
                                <i class="fas fa-check-circle ikonid-ok"style="display:none"></i>
                                 </small>
                            <input type="text" name="identitas" class="form-control " placeholder="" required>
                                                    
                       </div>
                       <div class="form-group">
                            <label for="alamat" class="mb-0 pb-0">alamat</label>
                           <input type="text" name="alamat" class="form-control" placeholder="Jl Diponegoro 01" >
                       </div>
                       <div class="form-group">
                            <label for="provinsi" class="mb-0 pb-0">Provinsi</label>
                            <select name="provinsi" class="form-control provinsi" data-live-search="true" title="Silahkan Pilih" required>
                                    <?php 
                                    foreach($provinsi as $de){
                                        echo '<option value="'.$de['prov_id'].'">'.$de['prov_name'].'</option>';
                                    }                                                                                            
                                    ?>                                    
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="kota" class="mb-0 pb-0">Kota</label>
                            <select name="kota" class="form-control kota" data-live-search="true" title="Silahkan Pilih" required>
                                <option disabled>silahkan isi provinsi</option>                                                                  
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="kec" class="mb-0 pb-0">Kec</label>
                            <select name="kec" class="form-control kec" data-live-search="true" title="Silahkan Pilih" required>
                                <option disabled>silahkan isi Kota</option>                                                                  
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="desa" class="mb-0 pb-0">Desa</label>
                            <select name="desa" class="form-control desa" data-live-search="true" title="Silahkan Pilih" required>
                                <option disabled>silahkan isi Kota</option>                                                                  
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="kodepos" class="mb-0 pb-0">Kodepos</label>
                           <input type="text" name="kodepos" class="form-control" placeholder="65141" >
                       </div>
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">telp</label>
                           <input type="tel" name="telp" class="form-control" placeholder="0812333111" >
                       </div>
                       <div class="form-group">
                            <label for="email" class="mb-0 pb-0">Email</label>
                            <small class="statusemail"> 
                                <div class="spinner-border spinner-border-sm  loadingiconemail" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="fas fa-times-circle ikonemail-gagal" style="display:none"> Email sudah terpakai</i>
                                <i class="fas fa-check-circle ikonemail-ok"style="display:none"></i>
                                 </small>
                           <input type="email" name="email" class="form-control" placeholder="contoh@gmail.com" >
                       </div>                        
                       <div class="form-group">
                            <label for="password" class="mb-0 pb-0">Password</label>
                           <input type="text" name="password" class="form-control" placeholder="Kosongi Jika Tidak Ingin Ganti Password" >
                       </div>
                       <div class="form-group">
                        <label for="kategori" class="mb-0 pb-0">jenis</label>
                           <select name="tipe" class="form-control" data-live-search="true" title="silahkan pilih" required>                                                                                               
                                                <option value="1">Member</option>                                                        
                                                <option value="2">Siswa</option>                                                        
                                                <option value="3">Guru</option>                                                        
                                                 </select>
                       </div>
                        <div class="form-group row">
                                <label for="status" class="mb-0 py-auto col-lg-1 col-sm-3">Status</label>
                                <div class="col">
                                    <input type="checkbox" name="status" value="1"  data-toggle="toggle" data-on="Aktif" data-off="Tidak Aktif" data-size="normal" data-width="200" data-onstyle="success" data-offstyle="danger">
                                </div>
                       </div>                                  
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  
  <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo base_url('panel/m/member_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Member</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>


<?= $this->endSection() ?>

<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js');?>"></script>

<script>

        // SET IDENTITAS NUMBER ONLY
        setInputFilter(document.getElementById("identitas"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
        }



        // check ID
        $('input[name=identitas]').change(function(){
           $('.statusid').removeClass('text-success').removeClass('text-danger');
            $('.loadingiconid').show();
            $('.ikonid-gagal').hide();
            $('.ikonid-ok').hide();
            var user= $(this).val();
            $('.tombolsubmit').hide();
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/m/checkmember');?>",
                data: {identitas: user}
                }).done(function(msg ) {
                        setTimeout(
                    function() 
                    {   
                        if(msg=='1'){
                            $('.statusid').addClass('text-success');
                            $('.loadingiconid').hide(); 
                            $('.ikonid-ok').show();
                            $('.tombolsubmit').show();
                        }else{
                            $('.statusid').addClass('text-danger');
                            $('.loadingiconid').hide(); 
                            $('.ikonid-gagal').show();
                            $('.tombolsubmit').hide();
                        }
                    }, 1000);
                
                });
            
        });
        // check email
        $('input[name=email]').change(function(){
           $('.statusemail').removeClass('text-success').removeClass('text-danger');
            $('.loadingiconemail').show();
            $('.ikonemail-gagal').hide();
            $('.ikonemail-ok').hide();
            var user= $(this).val();
            $('.tombolsubmit').hide();
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/m/checkmembermail');?>",
                data: {email: user}
                }).done(function(msg ) {
                        setTimeout(
                    function() 
                    {   
                        if(msg=='1'){
                            $('.statusemail').addClass('text-success');
                            $('.loadingiconemail').hide(); 
                            $('.ikonemail-ok').show();
                            $('.tombolsubmit').show();
                        }else{
                            $('.statusemail').addClass('text-danger');
                            $('.loadingiconemail').hide(); 
                            $('.ikonemail-gagal').show();
                            $('.tombolsubmit').hide();
                        }
                    }, 1000);
                
                });
            
        });

        function adduser(){
            resetform();
            $('#myModalAdd').modal('show')
        }
        function resetform(){

            $('.loadingicon').hide();
            $('.ikon-gagal').hide();
            $('.ikon-ok').hide();
            $('#form-tambah-user').trigger("reset");
            $('#form-edit-user').trigger("reset");
        }

        // get Edit Records
        $('#tabel-master-katalog').on('click','.edit_record',function(){

            resetform()
            var id=$(this).data('id');
                    $.ajax({
                    dataType: "json",
                    url: '<?php echo base_url('panel/m/member_detail/');?>/'+id,                
                    success:function(data) { 
                        //console.log(data.result);
                       
                        $('#form-update-user [name="id"]').val(data.result.id_member);
                        $('#form-update-user [name="nama"]').val(data.result.nama);
                        $('#form-update-user [name="identitas"]').val(data.result.identitas);
                        $('#form-update-user [name="alamat"]').val(data.result.alamat);
                        
                        $('#form-update-user [name="kodepos"]').val(data.result.kodepos);
                        $('#form-update-user [name="telp"]').val(data.result.telp);
                        $('#form-update-user [name="email"]').val(data.result.email);
                        $('#form-update-user [name="tipe"]').selectpicker('val',data.result.type);
                        if(data.result.status==1)
                            $('#form-update-user [name="status"]').bootstrapToggle('on')
                                else 
                            $('#form-update-user [name="status"]').bootstrapToggle('off')
                            $('.provinsi').val(data.result.provinsi);
                            $('.kota').val(data.result.kota);
                            $('.kec').val(data.result.kec);
                            getKota(data.result.provinsi).then(()=>{
                                    getKec(data.result.kota).then(()=>{
                                        getDesa(data.result.kec).then(()=>{
                                        //console.log('okkk');
                                        $('#form-update-user [name="provinsi"]').selectpicker('val',data.result.provinsi);
                                        $('#form-update-user [name="kota"]').selectpicker('val',data.result.kota);
                                        $('#form-update-user [name="kec"]').selectpicker('val',data.result.kec);
                                        $('#form-update-user [name="desa"]').selectpicker('val',data.result.desa);                                       
                                                 
                                            
                                        });
                                    });
                            });

                       $('#myModalUpdate').modal('show');
                    }
                    });       


           
                
            
        });

        $('#tabel-master-katalog').on('click','.delete_record',function(){
            var id=$(this).data('id');
            $('#ModalDelete').modal('show');
            $('#deleteform [name="id"]').val(id);
         });
 
    
        
        //  on provinsi select
        $('.provinsi').change(function(){
            
            var id=$(this).val();
            getKota(id);
        })

        //on kota select
        $('.kota').change(function(){
            
            var id=$(this).val();
            getKec(id);
        })

        //on Kec select
        $('.kec').change(function(){
            
            var id=$(this).val();
            // console.log($('select[name="desa"]').html());
            getDesa(id);
        })

        async function getKota(id){
            
            await $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/app/kota');?>",
                data: {prov_id: id}
                }).done(function(result ) {
                      
                    //  console.log($('select[name="kota"]').html());
                     $('select[name="kota"]').empty();
                    var kota=JSON.parse(result);     
                    // console.log(kota);               
                    for(var a=0;a<kota.length;a++){
                         
                        $('select[name="kota"]').append('<option value="'+kota[a]['city_id']+'">'+kota[a]['city_name']+'</option>');
                    }
                    $('select[name="kota"]').selectpicker('refresh');
                    //console.log('kota done');
                }).fail(function(xhr,status,error) {
                    alert( "error " );
                    console.log(error);
                })
                .always(function() {
                    
                });
        }
        async function getKec(id){
             // console.log($('select[name="kec"]').html());
             await $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/app/kec');?>",
                data: {city_id: id}
                }).done(function(result ) {
                      
                    //  console.log($('select[name="kec"]').html());
                     $('select[name="kec"]').empty();
                    var kota=JSON.parse(result);     
                    // console.log(kota);               
                    for(var a=0;a<kota.length;a++){
                         
                        $('select[name="kec"]').append('<option value="'+kota[a]['dis_id']+'">'+kota[a]['dis_name']+'</option>');
                    }
                    $('select[name="kec"]').selectpicker('refresh');
                   // console.log('Kec done');
                }).fail(function(xhr,status,error) {
                    alert( "error " );
                    console.log(error);
                })
                .always(function() {
                    
                });;
        }
        async function getDesa(id){
             // console.log($('select[name="kec"]').html());
             await $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/app/desa');?>",
                data: {dis_id: id}
                }).done(function(result ) {
                      
                  //   console.log($('select[name="desa"]').html());
                     $('select[name="desa"]').empty();
                    var kota=JSON.parse(result);     
                    // console.log(kota);               
                    for(var a=0;a<kota.length;a++){
                         
                        $('select[name="desa"]').append('<option value="'+kota[a]['subdis_id']+'">'+kota[a]['subdis_name']+'</option>');
                    }
                    $('select[name="desa"]').selectpicker('refresh');
                   // console.log('Desa done');
                }).fail(function(xhr,status,error) {
                    alert( "error " );
                    console.log(error);
                })
                .always(function() {
                    
                });;
        }

    $(document).ready(function(){
            // selectpicker
            $('select').selectpicker();

        // Setup datatables

        table = $('#tabel-master-katalog').DataTable({ 
            "language": 
                {
                "url" :"<?php echo base_url('assets/plugins/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo base_url('panel/usermemberajax')?>",
            
            columnDefs: [
                
                { targets: -1, className: 'dt-body-nowrap text-center'}, //last column center.
                { targets: 0, className: 'dt-body-nowrap text-center'}, //last column center.
                { targets: 1, className: 'dt-body-nowrap text-center'}, //last column center.
                { targets: -2, className: 'dt-body-nowrap text-center'}, //last column center.
                { targets: 7, className: 'dt-body-nowrap text-center'}, //last column center.
                { targets: 5, className: 'dt-body-nowrap text-center'}, //last column center.
                
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
            
            {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8]
                }
            },
            {  extend: 'excel',
                title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8]
                }
            },
            {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                    messageTop :'<?php echo $title.' '.date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6,7,8]
                }
            },
        ],
        });
    });


</script>
<?= $this->endSection() ?>