<section class="bg-primary">
  <div class="container">

<!--  START NAVBAR   -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark   navbar-offcanvas header">
      
      
<!--     If you want to show the button online on mobile use this toggle   
      
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>

-->
      
<!--      This toggle button is always visible  -->
     <button class="navbar-toggler d-block" type="button" id="navToggle">
      <span class="navbar-toggler-icon"></span>
    </button>

    <a class="navbar-brand mr-auto ml-2" href="#">LIMASYS - Library Management System </a>

      <div class="navbar-collapse offcanvas-collapse menu">
      <?php 
        $uri= current_url(true); 
        $segment ='';
        if($uri->getSegment(1)!=null)
        $segment= $uri->getSegment(1);
      ?>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item <?php if(  $segment=='home' ) echo 'active';?>">
            <a class="nav-link" href="<?php echo base_url();?>">Pencarian Buku </a>
          </li>
          <li class="nav-item <?php if(  $segment=='cek'  ) echo 'active';?>">
            <a class="nav-link" href="<?php echo base_url('cek');?>">Cek Pinjaman</a>
          </li>
          <li class="nav-item <?php if(  $segment=='koleksi_baru'  ) echo 'active';?>">
            <a class="nav-link" href="<?php echo base_url('koleksi_baru');?>">Koleksi Baru</a>
          </li>
          <!-- <li class="nav-item <?php if(   $segment=='login'  ) echo 'active';?>">
            <a class="nav-link" href="<?php echo base_url('login');?> ">Akun</a>
          </li> -->
          <li class="nav-item <?php if(   $segment=='informasi'  ) echo 'active';?>">
            <a class="nav-link" href="<?php echo base_url('informasi');?> ">Tentang Bookman</a>
          </li>
           
        </ul>
 
        
      </div>
    </nav>
<!--  END NAVBAR   -->

  </div>
  <!--/.container    -->

</section>