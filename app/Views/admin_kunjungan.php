
<?php $this->extend('admin/page_layout'); ?>

<?= $this->section('content') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Kunjungan</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Data Kunjungan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Kunjungan</h3>
                 
              </div>
             
              <!-- /.card-header -->
              <div class="card-body row">
                    <div class="col-8" >
                    
                        <form id="form-kunjungan" method="POST" action="#">
                        <div class="form-group row my-0 py-0">
                            <label for="tanggal" class="col-md-2 col-form-label ">Input Member</label>   
                            <div class="col-md-9">                            
                              <input type="hidden" name="idmember" class="id_member"  value="0">                         
                              <input type="text" name="member" class="form-control membersearch form-control-sm" onmouseover="this.focus();" placeholder="Nama Member" required >
                            
                            </div>                                              
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-outline-primary  btn-sm"   title="Tambah Kunjungan"  ><i class="fas fa-user" ></i></button> 
                            </div>
                        </div>
                        <div class="form-group row my-0 py-0">
                            <div class="col-md-12">
                                <div class="alert alert-success alert-ok" role="alert" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <span class="text-ok"><strong>Sukses!</strong> Data Ditambahkan!</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="alert alert-danger alert-gagal" role="alert" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <span class="text-gagal"><strong>Gagal!</strong> Data gagal Ditambahkan!</span>
                                </div>
                            </div>
                        </div>
                        </form>
                        
                    </div>
                    <div class="col-4">
                      <a target="_blank" href="<?=base_url('sys');?>" class="btn btn-transparent btn-sm text-info ml-3  float-right"><i class="fas fa-external-link-square-alt"> Link Kunjungan </i> </a>
                    </div>
                </div>
              <div class="card-body">
                
               
                    
                  
                    
                    
                </form>
                <table id="tabel-master-katalog" class="table table-bordered table-hover table-sm" width="100%">
                <thead>
                        <tr>
                        <th>No</th> 
                        <th>Waktu Kunjungan</th>
                        <th>Member</th>
                         
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


 
  
 



<?= $this->endSection() ?>

<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js');?>"></script>
 
<script>
     $('.membersearch').autocomplete({                    
                    source: "<?=base_url('panel/katalog/search');?>",
                    minLength: 2,
                    select: showResult,
                    focus : showResult, 
                    change: false
                    } );  
                function showResult( event, ui ) {
                            $('#id_member').val(ui.item.value);
                            $('.membersearch').val(ui.item.label);
                            
                            return false;
                }  

    $( "#form-kunjungan" ).submit(function( event ) {

      let id = $('#id_member').val();
        if(id == 0){
          alert('Isi Data Member Dengan benar');
          return false;
        }
      event.preventDefault();   

    });
    $(document).ready(function(){
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        
        table = $('#tabel-master-katalog').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/plugins/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo site_url('panel/kunjunganajax')?>",
            
            columnDefs: [
                
                { targets: 0, className: 'dt-body-nowrap text-center', width: '5%'}, //last column center.
                { targets: 1, className: 'dt-body-nowrap text-center', width: '20%'}, //last column center.
               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'excel',
                title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                    messageTop :'<?php echo $title.' '.date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
         ],
        });
 
            // end setup datatables

 

            
    });
</script>
<?= $this->endSection() ?>