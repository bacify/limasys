
<?php $this->extend('admin/page_layout'); ?>

<?= $this->section('content') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Daftar Peminjaman</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Daftar Peminjaman</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Peminjaman</h3>
                <button class="btn btn-success float-right" onclick="add_pinjam()">Pinjaman Baru</button>                 
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover table-sm" width="100%">
                <thead>
                        <tr>
                        <th>NoPinjam</th> 
                        <th>Tanggal</th>
                        <th>Member</th>
                        <th>handphone</th>
                        <th>email</th>
                        <th>status</th>                        
                        <th>Tindakan</th>                        
                         
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


     <!-- Modal Add Product-->
     <form id="form-pinjam-baru" action="<?php echo base_url('panel/p/new');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Pinjaman Baru </h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">                        
                       <div class="form-group">
                            <label for="judul" class="mb-0 pb-0">nomor Anggota</label>     
                            <small class="status"> 
                                <div class="spinner-border spinner-border-sm  loadingicon" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="fas fa-times-circle ikon-gagal" style="display:none"> Nomor Tidak Ditemukan</i>
                                <i class="fas fa-check-circle ikon-ok"style="display:none"></i>
                                 </small>                       
                            <input type="text" name="nomor" id="noidmember" class="form-control" maxlength="6" placeholder="123456" required>
                                                    
                       </div>   
                       <div class="form-group">
                            <label for="no_register" class="mb-0 pb-0">Nama</label>                           
                           <input type="text" id="namamember" name="nama" class="form-control" placeholder="nama" >
                       </div>                                                                      
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">Telp</label>
                            <input type="hidden"  name="truephone"    value="0">                       
                            <input type="tel" id="notel" name="phone" class="form-control" placeholder="0812345678">
                       </div>    
                         
                                                                                         
                                                                                        
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit" disabled>Pinjaman Baru</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>

       <!-- Modal delete Product-->
    <form id="deleteform" action="<?php echo base_url('panel/p/delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Pinjaman</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>


<?= $this->endSection() ?>

<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js');?>"></script>

<script>
     
  function add_pinjam() {
    resetform();
    $('#myModalAdd').modal('show');
    $('#myModalAdd').on('shown.bs.modal', function (e) {
      $('#noidmember').focus();
    });
    

    
  }

  function resetform(){
    $('.tombolsubmit').prop('disabled',true);
    $('.loadingicon').hide();
    $('.ikon-gagal').hide();
    $('.ikon-ok').hide();
    $('#form-pinjam-baru').trigger("reset");
    
  }

  $('#namamember').autocomplete({                    
                    source: '<?=base_url('panel/m/member_search');?>',
                    minLength: 2,
                    select: showResult,
                    focus : showResult, 
                    change: false
                    } );  
                function showResult( event, ui ) {
                            $('#noidmember').val(ui.item.value);
                            let vale = ui.item.label.split('|');
                            $('#namamember').val(vale[0].trim());
                            $('#notel').val(vale[1].trim());
                            $('.tombolsubmit').prop('disabled',false);
                            return false;
                }  
 // SET IDENTITAS NUMBER ONLY
    setInputFilter(document.getElementById("noidmember"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
        }

    $( "#noidmember" ).keyup(function() {
      var value=$(this).val();
      console.log(value);
      if(value.length == 6){
                $.ajax({
                        method: "GET",
                        dataType: "json",
                        url: "<?php echo base_url('panel/m/member_detail');?>/"+value,                        
                        })
                        .done(function( msg ) {

                          if(msg.status=='1'){
                            
                            $('#form-pinjam-baru input[name="nama"]').val(msg.result.nama);
                            $('#form-pinjam-baru input[name="phone"]').val(msg.result.telp);
                            $('.tombolsubmit').prop('disabled',false);
                          } else{
                            $('#form-pinjam-baru input[name="nama"]').val('');
                            $('#form-pinjam-baru input[name="phone"]').val('');
                            $('.tombolsubmit').prop('disabled',true);
                          }


                            return false;
                        });
        }
    });
    $(document).ready(function(){
            // selectpicker
            $('select').selectpicker();

        // Setup datatables

        table = $('#tabel-master-katalog').DataTable({ 
            "language": 
                {
                "url" :"<?php echo base_url('assets/plugins/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [['0','desc']], //init datatable not ordering
            ajax: "<?php echo base_url('panel/pinjamanajax')?>",
            
            columnDefs: [
                
                { targets: -1, className: 'dt-body-nowrap text-center bd-callout'}, //last column center.
                { targets: 5, className: 'dt-body-nowrap text-center'}, //last column center.
            
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
            
            {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
            },
            {  extend: 'excel',
                title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
            },
            {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                    messageTop :'<?php echo $title.' '.date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
            },
        ],
        });

        $('#tabel-master-katalog').on('click','.delete_record',function(){
              var id=$(this).data('id');
              $('#ModalDelete').modal('show');
              $('#deleteform [name="id"]').val(id);
            });
    });


</script>
<?= $this->endSection() ?>