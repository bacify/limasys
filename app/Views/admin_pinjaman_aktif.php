
<?php $this->extend('admin/page_layout'); ?>

<?= $this->section('content') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        <small><a href="<?php echo base_url('panel/home');?>" class="text-decoration-none alert-link"><i class="fas fa-chevron-circle-left"></i> kembali</a></small>
          <h1><?php if(isset($title)) echo $title; ?></h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?php if(isset($title)) echo $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
      <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?php if(isset($title)) echo $title; ?></h3>
                 
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                        <tr>
                        <th>No</th> 
                        <th>NoPinjam</th> 
                        <th>Tanggal</th> 
                        <th>judul</th>
                        <th>Pengarang</th>
                        <th>NoRegister</th>
                        <th>Member</th>
                        <th>telp</th>                        
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

<?= $this->endSection() ?>

<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js');?>"></script>

<script>
$(document).ready(function(){

        
// selectpicker
$('select').selectpicker();


// Setup datatables
table = $('#tabel-master-katalog').DataTable({ 
    "language": 
        {
        "url" :"<?php echo base_url('assets/plugins/datatables/lang/Indonesian.json');?>" 
        },
    processing: true,
    serverSide: true,
    responsive: true,
    order: [['1','desc']], //init datatable not ordering
    ajax: "<?php echo base_url('panel/daftar_pinjaman_ajax')?>",
    
    columnDefs: [
        
        { targets: 0, className: 'dt-body-nowrap text-center',orderable: false}, //last column center.
        { targets: 1, className: 'dt-body-nowrap text-center'}, //last column center. 
        { targets: 2, className: 'dt-body-nowrap text-center'}, //last column center. 
        { targets: -1, className: 'dt-body-nowrap text-center'}, //last column center. 
    
    
        
    ],
    "dom": 'Bfrtip',
    buttons: [             
    
    {  extend: 'copy',                
        className: 'btn btn-default',
        exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6 ]
        }
    },
    {  extend: 'excel',
        title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
        className: 'btn btn-default',
        exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6 ]
        }
    },
    {  extend: 'print',                
        className: 'btn btn-default',
        exportOptions: {
            title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
            messageTop :'<?php echo $title.' '.date('d-m-Y'); ?> ',
            columns: [ 0, 1, 2, 3,4,5,6 ]
        }
    },
],
});




   
        
            
    
});
</script>
<?= $this->endSection() ?>