
<?php $this->extend('admin/page_layout'); ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        <small><a href="<?php echo base_url('panel/inventory');?>" class="text-decoration-none alert-link"><i class="fas fa-chevron-circle-left"></i> kembali</a></small>
          <h1>Detail Transaksi (<strong><?php echo sprintf('%06d', $id);?></strong>) </h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Detail Transaksi  (<?php echo sprintf('%06d', $id);?>)</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                    <div class="col-9">
                    <form method="POST" action="<?php echo base_url('panel/i/add_detail');?>">
                    <div class="form-group row my-0 py-0">
                        <label for="notransksaksi" class="col-md-2 col-form-label">NoTransaksi</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="notransksaksi" value="<?php echo sprintf('%06d', $id);?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="asal" class="col-md-2 col-form-label">Asal</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="asal" value="<?php echo $master['asal'];?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="keterangan" class="col-md-2 col-form-label">Keterangan</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="keterangan" value="<?php echo $master['keterangan'];?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-2 col-form-label">Tanggal</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="tanggal" value="<?php echo $master['created_at'];?>">
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-2 col-form-label ">Dt katalog</label>                        
                        <div class="col-md-6">
                        <input type="hidden" name="id_katalog" class="id_katalog"  value="0">
                        <input type="hidden" name="id_master" value="<?php echo $id;?>">
                        <input type="text" name="judul" class="form-control mainsearch form-control-sm" placeholder="Ketik Judul,Pengarang atau keyword lainnya" required <?php if($master['status_transaksi']!='0') echo 'disabled';?>>
                        <small id="emailHelp" class="form-text text-muted">*hanya untuk katalog yang sudah tersimpan di database.</small>
                        </div>
                        <div class="col-md-2">
                        <input type="number" name="jumlah" class="form-control form-control-sm"  placeholder="jumlah"  required <?php if($master['status_transaksi']!='0') echo 'disabled';?>>                        
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-outline-primary  btn-sm"   title="Tambah Data Katalog" <?php if($master['status_transaksi']!='0') echo 'disabled';?>><i class="fas fa-database" ></i></button> 
                        </div>
                    </div>
                    </form>
                         
                    </div>
                    <div class="col-3">
                    <div class="float-right">                        
                        <button class="btn btn-primary btn-sm btn-save" data-toggle="modal" data-target="#myModalsave" title="Proses Data Utama" <?php if($master['status_transaksi']!='0') echo 'disabled';?>> <i class="fas fa-cloud" > Simpan</i></button>
                        <?php if($master['jenis_transaksi']== 1): ?>
                        <button class="btn btn-success btn-sm " data-toggle="modal" data-target="#myModalAdd"  title="Tambah Buku Baru" <?php if($master['status_transaksi']!='0') echo 'disabled';?>><i class="fas fa-plus" > Tambah</i></button> 
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                 
                <table id="tabel-master-katalog" class="table table-bordered table-hover table-sm" width="100%">
                <thead>
                <tr>
                        <th>No</th>
                        <th>no_register</th>
                        <th>no_panggil</th>
                        <th>judul</th>
                        <th>pengarang</th>
                        <th>penerbit</th>                        
                        <th>kategori</th>
                        <?php if($master['jenis_transaksi']== 2) echo '<th>Stok</th>';?>
                        <th>Jumlah  </th>
                        <?php if($master['status_transaksi']== 0) echo '<th class="text-center"><i class="fas fa-tools "></i></th>';?>
                        
                        
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


 <!-- Modal Add Product-->
 <form id="form-tambah-buku" action="<?php echo site_url('panel/i/add_katalog');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Tambah Buku Baru</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <!-- <div class="form-group mb-0">

                        <small><label for="no_register" class="mb-0 pb-0">Nomor Register</label></small>
                             
                           <input type="text" name="no_register" class="form-control" placeholder="Nomor Register">
                       </div> -->
                       <div class="form-group mb-0">
                       <small><label for="judul" class="mb-0 pb-0">Judul</label></small>
                           <input type="text" name="judul" class="form-control inputjudulbuku" placeholder="Judul Buku" required>
                           <input type="hidden" name="id_master" value="<?php echo $id;?>">
                       </div>
                       <div class="form-group mb-0">
                       <small><label for="no_panggil" class="mb-0 pb-0">No Panggil</label></small>
                           <input type="text" name="no_panggil" class="form-control" placeholder="Nomor Panggil (ex: '793.7 Woe k')" >
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="pengarang" class="mb-0 pb-0">pengarang</label></small>
                           <input type="text" name="pengarang" class="form-control" placeholder="Pengarang" required>
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="penerbit" class="mb-0 pb-0">Penerbit</label></small>
                           <input type="text" name="penerbit" class="form-control" placeholder="penerbit" required>
                       </div>
                       <div class="form-group mb-0">
                       <small><label for="kategori" class="mb-0 pb-0">Kategori</label></small>
                           <select name="kategori" class="form-control" data-live-search="true" title="Jenis Katalog" required>
                                         
                                                      <?php foreach ($kategori as $row) :?>
                                                            <option value="<?php echo $row['id_kategori'];?>"><?php echo $row['nama_kategori'];?></option>
                                                        <?php endforeach;?>
                                                 </select>
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="bahasa" class="mb-0 pb-0">Bahasa</label></small>
                            <select name="bahasa" class="form-control" data-live-search="true" title="Bahasa" required>
                                    
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Asing">Asing</option>
                                    
                            </select>
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="tahun" class="mb-0 pb-0">Tahun</label></small>
                           <input type="text" name="tahun" class="form-control" placeholder="tahun" required>
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="edisi" class="mb-0 pb-0">Edisi</label></small>
                           <input type="text" name="edisi" class="form-control" placeholder="Edisi" required>
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="subyek" class="mb-0 pb-0">Subyek</label></small>
                           <input type="text" name="subyek" class="form-control" placeholder="Subyek" required>
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="klasifikasi" class="mb-0 pb-0">Klasifikasi</label></small>
                           <input type="text" name="klasifikasi" class="form-control" placeholder="klasifikasi (ex: 793.7)" >
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="deskripsi" class="mb-0 pb-0">Deskripsi</label></small>
                           <input type="text" name="deskripsi" class="form-control" placeholder="Deskripsi Fisik (ex: 'xii, 200 hal. : ill. ; 18 cm)'" >
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="isbn" class="mb-0 pb-0">isbn</label></small>
                           <input type="text" name="isbn" class="form-control" placeholder="isbn" >
                       </div>
                       <div class="form-group mb-0">
                        <small><label for="isbn" class="mb-0 pb-0">Jumlah</label></small>
                           <input type="text" name="jumlah" class="form-control" placeholder="0" required>
                       </div>
                                         
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
 <!-- Modal save Product-->
 <form id="saveform" action="<?php echo base_url('panel/i/save_detail');?>" method="post" >
         <div class="modal fade" id="myModalsave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Simpan dan Proses Transaksi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <input type="hidden" name="id_master" value="<?php echo $id;?>" >
                        <p><strong class="text-warning bg-warning text-uppercase">Transaksi tidak akan dapat di ubah jika sudah di simpan</strong>, Apakah anda yakin akan menyimpan data ini sekarang?</p>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit"   value="submit" class="btn btn-primary buttonsave">Simpan</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 <!-- Modal delete Product-->
 <form id="deleteform" action="<?php echo base_url('panel/i/delete_detail');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Data</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                                                <input type="hidden" name="id_master" value="<?php echo $id;?>" >
                                                <input type="hidden" name="id_detail" class="form-control" >
                                                 <p> Apakah anda yakin akan menghapus data ini sekarang?</p>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  

     <?= $this->endSection() ?>

<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js');?>"></script>
<script>
   
    // $(".buttonsave").click(function(){
        
    //     $(this).prop("disabled",true);
    // });

   
    function updateJumlah(x) {
                
                var newval=$(x).val();
                var iddetail=$(x).closest('td').find('.inputiddetail').val();
                var jenis = <?=$master['jenis_transaksi'];?>;  
                <?php if($master['jenis_transaksi']==2):?>              
                    var stok=$('.istok').val();
                    if(stok<newval){
                        table.ajax.reload(null,false);
                        alert('Pastikan jumlah tidak melebihi stok yang ada')
                        return false;
                    }
                <?php endif; ?>
                if(!isNaN(newval)){
                    // proces data
                    //console.log(newval +' '+iddetail+' '+jenis);
                    let data;
                    if(jenis == 1){
                        data ={
                            id_detail : iddetail,
                            masuk : newval
                        };                        
                    }else{
                        data ={
                            id_detail : iddetail,
                            keluar : newval
                        };
                    }
                    $.ajax({	
                        url: "<?=base_url('panel/i/update_detail');?>",
                        data: data,
                        type: 'POST'
                        })
                        .done(function(data){        
                            console.log(data);  
                            let d = JSON.parse(data);
                            if(d.status == 1){
                                console.log(data);
                                alert( d.text );                                 
                            }else{
                                alert(d.text);
                            }
                        })
                        .fail(function(xhr,status,error) {
                            alert( "error "+error );
                            console.log(error);
                        })
                        .always(function() {
                            table.ajax.reload(null,false);
                        });
                   
                     
                }else{
                    $(x).val(0);
                    alert('Masukkan jumlah yang benar');
                    return false;
                }
                
                        
    }
    $(document).ready(function(){


        
                
        // selectpicker
        $('select').selectpicker();



        
         table = $('#tabel-master-katalog').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/plugins/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo base_url('panel/idetailajax/'.$id)?>",
            
            columnDefs: [
                
                { targets: -1, className: 'text-nowrap dt-body-nowrap text-center'}, //last column center.
                { targets: -2, className: 'text-nowrap dt-body-nowrap text-center'}, //last column center.
                 
               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7 ]
                }
             },
             {  extend: 'excel',
                title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7 ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                    messageTop :'<?php echo $title.' '.date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6,7 ]
                }
             },
         ],
        });


       
            // end setup datatables


             



             
            // End Edit Records
            // get Save Records
            $('#tabel-master-katalog').on('click','.save_record',function(){
                var id=$(this).data('id');
                $('#ModalSave').modal('show');
                
            });
            // End delete Records
            // get Save Records
            $('#tabel-master-katalog').on('click','.delete_record',function(){
                var id=$(this).data('id');
               
                $('#ModalDelete').modal('show');
                $('#deleteform [name="id_detail"]').val(id);
            });
            // End delete Records
            
                  

               let url = "<?php if($master['jenis_transaksi']==1 ) 
                            echo base_url('panel/katalog/search');
                            else
                            echo base_url('panel/katalog/searchinstock');?>";
              $('.mainsearch').autocomplete({                    
                    source: url,
                    minLength: 2,
                    select: showResult,
                    focus : showResult, 
                    change: false
                    } );  
                function showResult( event, ui ) {
                            $('.id_katalog').val(ui.item.value);
                            $('.mainsearch').val(ui.item.label);
                            
                            return false;
                }  


                
                    
            
    });
</script>


<?= $this->endSection() ?>
