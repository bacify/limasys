<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Main::index');
$routes->add('/home', 'Main::home');
$routes->get('/cari/(:segment)', 'Main::cari/$1');
$routes->get('/login', 'Login::index');
$routes->post('/login/auth', 'Login::auth');
$routes->get('/panel', 'Login::admin');
$routes->post('/pauth', 'Login::admin_auth');


$routes->get('/sys', 'Main::kunjungan');
$routes->get('/sys/insert/(:segment)', 'Main::kunjungan_add/$1');
$routes->add('/cek', 'Main::cek_pinjaman');
$routes->add('cek_pinjaman/(:segment)', 'Main::cek_pinjaman_member/$1');
$routes->get('koleksi_baru', 'Main::new_koleksi');
$routes->get('informasi', 'Main::informasi');

$routes->group('panel',['filter' => 'auth'], function($routes){
	$routes->get('home', 'Admin::index');	
	$routes->get('kategori', 'Admin::mkatalog_kategori');
	$routes->post('kategori_save', 'Admin::mkatalog_kategori_save');
	$routes->post('kategori_delete', 'Admin::mkatalog_kategori_delete');
	$routes->get('kategoriajax', 'Admin::mkatalog_kategori_ajax');
	$routes->get('katalog', 'Admin::mkatalog');
	$routes->get('katalogajax', 'Admin::katalogajax');
	$routes->post('new_katalog', 'Admin::katalog_new');
	$routes->post('remove_katalog', 'Admin::katalog_del');
	$routes->get('katalogjson/(:segment)', 'Admin::getkatalogjson/$1');	
	$routes->get('stok_katalog', 'Admin::katalog_stok');
	$routes->get('katalogstokajax', 'Admin::katalogstokajax');
	$routes->get('barcode', 'Admin::barcode');
	$routes->get('getbarcode/(:segment)', 'Admin::getbarcode/$1');
	$routes->get('getqrcode/(:segment)', 'Admin::getqrcode/$1');
	$routes->get('inventory', 'Admin::inventory');
	$routes->post('new_inventory', 'Admin::inventory_new');
	$routes->post('delete_inventory', 'Admin::inventory_delete');
	$routes->post('update_inventory', 'Admin::inventory_update');
	$routes->get('inventoryajax', 'Admin::inventoryajax');
	$routes->get('inventoryjson/(:segment)', 'Admin::getinventoryjson/$1');
	$routes->get('i/detail/(:segment)', 'Admin::inventory_detail/$1');
	$routes->get('idetailajax/(:segment)', 'Admin::inventorydetailajax/$1');
	$routes->get('katalog/search', 'Admin::cari_katalog');
	$routes->get('katalog/searchinstock', 'Admin::cari_katalog_instock');	
	$routes->post('i/add_detail', 'Admin::detail_new');
	$routes->post('i/update_detail', 'Admin::detail_update');
	$routes->post('i/delete_detail', 'Admin::detail_delete');	
	$routes->post('i/add_katalog', 'Admin::detail_katalog_new');
	$routes->post('i/save_detail', 'Admin::detail_save');
    $routes->get('riwayat_inventory', 'Admin::inventory_history'); 
	$routes->get('rinventoryajax', 'Admin::inventory_history_ajax');
	
	$routes->get('kunjungan', 'Admin::kunjungan');
	$routes->get('kunjunganajax', 'Admin::kunjunganajax');
	
	$routes->get('pinjaman', 'Admin::pinjaman_member');
	$routes->get('pinjaman', 'Admin::pinjaman_member');
	$routes->get('pinjamanajax', 'Admin::pinjaman_member_ajax');
	$routes->post('p/new', 'Admin::pinjaman_baru');
	$routes->post('p/delete', 'Admin::pinjaman_hapus');
	$routes->get('p/detail/(:segment)', 'Admin::pinjaman_detail/$1');
	$routes->post('p/detail_add', 'Admin::pinjaman_detail_add');
	$routes->post('p/proses_pinjam', 'Admin::pinjaman_detail_proses');
	$routes->post('p/return_pinjam', 'Admin::pinjaman_detail_return');
	$routes->post('p/detail_delete', 'Admin::pinjaman_detail_delete');
	$routes->get('p/detail_ajax/(:segment)', 'Admin::pinjaman_detail_ajax/$1');
	$routes->post('p/detail_add_ajax', 'Admin::pinjaman_detail_add_ajax');
	$routes->get('pengembalian', 'Admin::pengembalian_member');	
	$routes->get('r/pengembalian_ajax', 'Admin::pengembalian_history_ajax');	
	$routes->post('r/proses_pengembalian', 'Admin::pengembalian_proses');	
	$routes->post('r/submit_pengembalian', 'Admin::pengembalian_submit');	
	$routes->get('member_status/(:segment)', 'Admin::status_pinjaman/$1');	
	$routes->get('daftar_pinjaman', 'Admin::pinjaman_buku_list');	
	$routes->get('daftar_pinjaman_ajax', 'Admin::pinjaman_buku_list_ajax');	

	$routes->get('admin_list', 'Admin::user_admin');
	$routes->get('useradminajax', 'Admin::user_admin_ajax');
	$routes->post('u/checkadmin', 'Admin::admin_username_check');	
	$routes->post('u/admin_add', 'Admin::admin_user_add');
	$routes->post('u/admin_delete', 'Admin::admin_user_delete');

	$routes->get('member_list', 'Admin::user_member');
	$routes->get('usermemberajax', 'Admin::user_member_ajax');
	$routes->post('m/checkmember', 'Admin::member_username_check');	
	$routes->post('m/checkmembermail', 'Admin::member_email_check');	
	$routes->post('m/member_add', 'Admin::member_user_add');
	$routes->post('m/member_update', 'Admin::member_user_add');
	$routes->post('m/member_delete', 'Admin::member_user_delete');
	$routes->get('m/member_detail/(:segment)', 'Admin::member_user_json/$1');
	$routes->get('m/member_search', 'Admin::cari_member');


	$routes->get('setting', 'Admin::setting');

	$routes->post('app/kota', 'Admin::app_kota');
	$routes->post('app/kec', 'Admin::app_kecamatan');
	$routes->post('app/desa', 'Admin::app_desa');

	$routes->add('logout/', 'Login::admin_logout');    
     
	
});
 

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
